/************************************************************************************** 
Name              : WorkhoursTriggerHandler
Description       : One location to trigger all the triggers related to Workhours object
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mendy               21/08/2014      			    Dana          		[SW-10006]
2. Nevo                28/08/2017                   Dane                [SW-25649]
****************************************************************************************/
public  class WorkhoursTriggerHandler 
{
	public void AbsenceReasonValidation (list<Work_hours__c> WorkhoursNewList)
	{
		try
		{
	        map<string, string> ServiceWiseDayOffProjectMap = new map<string, string>();
	        list<ServiceWiseDayOffProject__c> ServiceWiseDayOffProjectList = [select name, ProjectId__c from ServiceWiseDayOffProject__c];
	        if (ServiceWiseDayOffProjectList != null && ServiceWiseDayOffProjectList.size() > 0)
	        {
	            for (ServiceWiseDayOffProject__c sdf : ServiceWiseDayOffProjectList)
	                 ServiceWiseDayOffProjectMap.put(sdf.ProjectId__c, sdf.name);
	        }
	        
	        for (Work_hours__c wh : WorkhoursNewList)
	        {
        		string strForCheck = string.valueof(wh.Project_name__c).substring(0,15);
		        if (ServiceWiseDayOffProjectMap.containsKey(strForCheck) && wh.AbsenceReason__c == null)
		        {
		            wh.addError('Need to add Absence Reson');
		        }
	        }
		}
		catch (exception e)
		{
			WorkhoursNewList[0].addError('ERROR : ' + e.getMessage());
		}
	}

	//Combining All WH Triggers from here Down.
	public void WorkHours_PopulateAccountFromProject (list<Work_hours__c> WorkhsList)
	{	
		Set<Id> projIdSet = new Set<Id>();
		Map<Id,SFDC_Project__c> projMap = new Map<Id,SFDC_Project__c>();
	
		for(Work_hours__c wh: WorkhsList)        
		{        	 	        			    	                                                                          
			if(wh.Project_name__c != null)
			{
				projIdSet.add(wh.Project_name__c);	
			}
		}        		                                           
				
		if(!projIdSet.isEmpty())
		{   
			projMap = new Map<Id,SFDC_Project__c>([select Id,Account__c from SFDC_Project__c where id IN : projIdSet]); 
			if(!projMap.isEmpty())
			{
				for(Work_hours__c wh: WorkhsList) 
				{
					if(projMap.containsKey(wh.Project_name__c))
					{
						wh.Account__c = projMap.get(wh.Project_name__c).Account__c;
						system.debug('Project: '+wh.Project_name__c + ' Account: '+ wh.Account__c);
					}					      
				}
			}
		}   


	}

	public void CalculateWorkingHoursOnIssue (list<Work_hours__c> WhList, list<Work_hours__c> WhOldList, map<Id, Work_hours__c> WhOldMap)
	{	
		List<Id> issues = new List<Id>();
		if (Trigger.isInsert || Trigger.isUnDelete)
		{
			for (Work_hours__c wh : WhList) if (wh.Issue_Number__c != null && wh.Hours__c > 0) issues.add(wh.Issue_Number__c);
		}
		else if (Trigger.isUpdate) 
		{
			for (Work_hours__c wh : WhList) 
			{
				if (wh.Issue_Number__c != null) issues.add(wh.Issue_Number__c);
				if (WhOldMap.get(wh.Id).Issue_Number__c != null && WhOldMap.get(wh.Id).Issue_Number__c != wh.Issue_Number__c) issues.add(WhOldMap.get(wh.Id).Issue_Number__c);
			}
		}
		else if (Trigger.isDelete)
		{
			for (Work_hours__c wh : WhOldList)
			{
				if (wh.Issue_Number__c != null && wh.Hours__c > 0)
				{
					issues.add(wh.Issue_Number__c);
				}
			}
		}
	
		AggregateResult[] groupedResults = [SELECT Issue_Number__c,SUM(Hours__c) FROM Work_hours__c where Issue_Number__c IN : issues GROUP BY Issue_Number__c];
		Map<Id,Decimal> actual_times = new Map<Id,Decimal>();
		for (AggregateResult groupedResult : groupedResults) actual_times.put((ID)groupedResult.get('Issue_Number__c'),(Decimal)groupedResult.get('expr0'));
	
		List <SFDC_Issue__c> updated_issues = [select Id,Actual_Time_Dev__c from SFDC_Issue__c where Id IN : actual_times.keySet()];
		for (SFDC_Issue__c updated_issue : updated_issues) updated_issue.Actual_Time_Dev__c = actual_times.get(updated_issue.Id);
	
		update updated_issues;

	}
 
    
	public void PreventReportingWH_AfterTheEndOfMonth (list<Work_hours__c> WhList,  map<Id, Work_hours__c> WhOldMap)
	{  
	   
	   Map<String,date> monthToLastday = new Map<String,date>();
	   for(Working_Hour_Standard__c whStnd : [select id,Last_day_to_report__c,Name from Working_Hour_Standard__c ])
	   {
	     monthToLastday.put(whStnd.Name , whStnd.Last_day_to_report__c);
	   }

	   for (Work_hours__c wh : WhList) 
			{ 
			     String monthName = getMonthName(wh.Date__c.month());
				 Date StandardDate = monthToLastday.get(monthName);
				if (     
				    (Trigger.isUpdate && WhOldMap.get(wh.Id).Hours__c != wh.Hours__c) ||
				    (Trigger.isUpdate && WhOldMap.get(wh.Id).Date__c != wh.Date__c) || 
					(Trigger.isInsert && wh.Hours__c != null) 
				   )
					{ 				   
					   if(StandardDate < System.today())
					   {
						   wh.Delayed_reporting__c = true;
					   }else{
				           wh.Delayed_reporting__c = false;
					   }
					} 
				  
			}
	}

	public string getMonthName(integer monthNum)
	{
	   if(monthNum == 1) return 'January';
	   if(monthNum == 2) return 'February';
	   if(monthNum == 3) return 'March';
	   if(monthNum == 4) return 'April';
	   if(monthNum == 5) return 'May';
	   if(monthNum == 6) return 'June';
	   if(monthNum == 7) return 'July';
	   if(monthNum == 8) return 'August';
	   if(monthNum == 9) return 'September';
	   if(monthNum == 10) return 'October';
	   if(monthNum == 11) return 'November';
	   if(monthNum == 12) return 'December';
	   return '';
	}
}