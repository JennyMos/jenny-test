/************************************************************************************** 
Name              : CreateIssueFromEmail
Description       : Create email to Help desk
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Dana Furman               02/01/2014          Kfir Cohen           [SW-7270]
****************************************************************************************/
global class CreateIssueFromEmail implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        SFDC_Issue__c newIssue = new SFDC_Issue__c(); //contact
        
   try
       {     
        String emailbody  = '';
        String[] myPlainText ;
        // Add the email plain text into the local variable
        try
            {
             emailbody  = email.plainTextBody.substring(0, email.plainTextBody.indexOf('<stop>'));
             myPlainText = email.plainTextBody.split('\n');            
            }
            catch (System.StringException e)
            {
                emailbody  = email.plainTextBody;
                System.debug('No <stop> in email: ' + e);   
            }    
         
         String IssueName;
          
        if(email.subject.length() > 79)
        {            
            IssueName = email.subject.SubString(0,79);      
            newIssue.SFDC_Issue_Name__c = IssueName;
            newIssue.Full_Name__c = email.subject;
        }
        else
        {
            newIssue.SFDC_Issue_Name__c = email.subject;
            newIssue.Full_Name__c = email.subject;
        }
        system.debug('email.subject: ' + newIssue.SFDC_Issue_Name__c);
        
        for(Integer i=0; i < myPlainText .size(); i++)
            {
                
                newIssue.SFDC_Issue_Description__c += myPlainText[i] + '\n' ;
            }

        system.debug('email.fromAddress: ' + email.fromAddress);
                
        //Try to get contact from the email
        try
            {
            Contact vCon = [Select Id, Name, Email  From Contact Where Email = :email.fromAddress Limit 1];
            if (vCon !=Null)
                newIssue.Requested_By__c = vCon.id; //Contact Lookup - Email from Address
            system.debug('Contact Lookup: ' + newIssue.Requested_By__c);
            }
            catch (System.QueryException e1)
            {
                System.debug('Contact execption');   
            }
        
        
        RecordType RecordType1 = new RecordType();
        try 
        {
            RecordType1  = [select Id from RecordType where Name = 'Customer Success' and SobjectType = 'SFDC_Issue__c'];
            if (RecordType1  != Null) 
              newIssue.RecordType  = RecordType1;   
        }
        catch (System.QueryException e1){
            System.debug('RecordType execption');  
        }
        //newIssue.RecordType = 'Help Desk' ; 
        
        Group queues1 = new Group();
        try
            {
           queues1 = [select Id,Name from Group where Name = 'Help Desk' and Type = 'Queue' Limit 1];
            if (queues1 != Null) 
                newIssue.OwnerId = queues1.id; //Help Desk Queue: 00GM0000000mJCT     
                system.debug('Owner: ' + newIssue.OwnerId);
                    }
            catch (System.QueryException e1)
            {
                System.debug('Owner execption');   
            }
           
            newIssue.SFDC_Issue_Status__c = '0 - Open';
            newIssue.Issue_Type__c = 'Help Desk';
            newIssue.Issue_Sub_Type__c = 'Other';
            newIssue.Issue_Source__c = 'Customer Success Email';
            
        Contact helpDeskCon = new Contact();
        try 
        {
            helpDeskCon = [Select Id, Name, AccountId, Email From Contact Where Name = 'Help Desk' Limit 1];
            if (helpDeskCon != Null)
                newIssue.Assigned_To__c = helpDeskCon.id ; //Help Desk Contact (still to be created)
            System.debug('Help desk contact is: ' + newIssue.Assigned_To__c);  
         }
        catch (System.QueryException e1)
            {
                System.debug('No help Desk Contact execption');   
            }
        
        
        newIssue.SFDC_Issue_Priority__c = 'Low';
        
        SFDC_Project__c project1 = new SFDC_Project__c();
        try 
        {
            project1 = [Select id From SFDC_Project__c Where Account__c =: helpDeskCon.AccountId AND Project_Type__c = 'Help Desk' Limit 1];
            if (project1 != Null)
                newIssue.SFDC_Project__c = project1.id; //According to Email address -> Account-> Helpdesk Project (Need to take care of the fact that an account can have multiple Help desk projects)
            system.debug('Project: ' + newIssue.SFDC_Project__c);
        }
        catch (System.QueryException e1)
            {
                System.debug('No project execption');   
            }
            
        newIssue.Start_Date__c = system.Today();
        newIssue.Due_Date__c = system.Today();  
            
         newIssue.Notification__c = True;
            
        // Insert the new Issue
        insert newIssue;
        System.debug('New Issue Object: ' + newIssue );
        
    }
       catch (Exception e) {
        String  errorMsg = e.getMessage();
        errorMsg += '\nStack Trace:' + e.getStackTraceString() + '\nLine Number:' + e.getLineNumber() + '\nException Type:' + e.getTypeName();         
        System.debug('errorMsg: ' + errorMsg);
    }
    
     if (newIssue== null) {
            result.message = 'Could not create issue from: ' + email.fromAddress;
            result.success = false;
            return result;
     }
    result.success = true;
    return result;
        
    }


}