public class ProjectWrapper {
	 @AuraEnabled public Boolean isCompleted {get;set;}
     @AuraEnabled public String Name {get;set;}
     @AuraEnabled public Id ProjectId {get;set;}
     @AuraEnabled public Id AccountId {get;set;}
     @AuraEnabled public String AccountName {get;set;}
     @AuraEnabled public Double TotalPerProject {get;set;}
     @AuraEnabled public Integer RowSpan {get;set;}
     @AuraEnabled public List<WorkingHourWrapper> SundayWH;
     @AuraEnabled public List<WorkingHourWrapper> MondayWH;
     @AuraEnabled public List<WorkingHourWrapper> TuesdayWH;
     @AuraEnabled public List<WorkingHourWrapper> WednesdayWH;
     @AuraEnabled public List<WorkingHourWrapper> ThursdayWH;
     @AuraEnabled public List<WorkingHourWrapper> FridayWH;
     @AuraEnabled public List<WorkingHourWrapper> SaturdayWH;
     @AuraEnabled public Integer WHID;
     @AuraEnabled public String color {get;set;}
     @AuraEnabled public List<WorkingHourWrapper> firstLineAllWeekWH {get;set;}
     @AuraEnabled public List<List<WorkingHourWrapper>> restAllWeekWH {get;set;}
     @AuraEnabled public Date SundayD {get;set;}
     @AuraEnabled public Boolean Error {get;set;}
        
     public ProjectWrapper(Boolean isCompleted,List<Work_hours__c> allWH, String AccountName, String Name, Id ProjectId, Date SundayD, Id UserId, Id AccountId,Boolean Error)
        {
            this.Name = Name;
            this.AccountName = AccountName;
            this.AccountId = AccountId;
            this.ProjectId = ProjectId;
            this.isCompleted = isCompleted;
            this.SundayD = SundayD;
            WHID = 1;
            SundayWH = new List<WorkingHourWrapper>();
            MondayWH = new List<WorkingHourWrapper>();
            TuesdayWH = new List<WorkingHourWrapper>();
            WednesdayWH = new List<WorkingHourWrapper>();
            ThursdayWH = new List<WorkingHourWrapper>();
            FridayWH = new List<WorkingHourWrapper>();
            SaturdayWH = new List<WorkingHourWrapper>();
            for(Work_hours__c wh : allWH){
                if(wh.Project_name__c == ProjectId){
                    if(wh.Date__c == SundayD){
                        SundayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(1)){
                        MondayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(2)){
                        TuesdayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(3)){
                        WednesdayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(4)){
                        ThursdayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(5)){
                        FridayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                    if(wh.Date__c == SundayD.addDays(6)){
                        SaturdayWH.add(new WorkingHourWrapper(isCompleted,wh.Issue_Number__c,this.Name,this.AccountId,wh.Id,WHID,wh.Project_name__c,wh.Hours__c,wh.Description__c,wh.Billable__c,wh.Vacation__c,wh.Sickness__c,wh.Travel_way__c,wh.AbsenceReason__c,wh.Date__c,wh.Internal_Comments__c,wh.Customer_Description__c));
                        WHID++;
                    }
                }
            }
            calcRowSpan();
            fillInWH(SundayD,UserId); 
            setWeekWH(); 
            calcTotalPerProject();
            
        }
    	
        public void setWeekWH()
        {
            firstLineAllWeekWH = new List<WorkingHourWrapper>();
            firstLineAllWeekWH.add(SundayWH[0]);
            firstLineAllWeekWH.add(MondayWH[0]);
            firstLineAllWeekWH.add(TuesdayWH[0]);
            firstLineAllWeekWH.add(WednesdayWH[0]);
            firstLineAllWeekWH.add(ThursdayWH[0]);
            firstLineAllWeekWH.add(FridayWH[0]);
            firstLineAllWeekWH.add(SaturdayWH[0]);
            
            restAllWeekWH = new List<List<WorkingHourWrapper>>();

            for(Integer i=1; i < RowSpan;i++){
                List<WorkingHourWrapper> weekWH = new List<WorkingHourWrapper>();
                weekWH.add(SundayWH[i]);
                weekWH.add(MondayWH[i]);
                weekWH.add(TuesdayWH[i]);
                weekWH.add(WednesdayWH[i]);
                weekWH.add(ThursdayWH[i]);
                weekWH.add(FridayWH[i]);
                weekWH.add(SaturdayWH[i]);
                restAllWeekWH.add(weekWH);
            }
        }
        
        public void calcTotalPerProject()
        {	
            Map<Integer,Double> whMAp = new Map<Integer,Double>();
            for(WorkingHourWrapper whw:firstLineAllWeekWH){
                whMAp.put(whw.WHID, whw.Hours);
            }
            for(List<WorkingHourWrapper> whwlist:restAllWeekWH){
                for(WorkingHourWrapper whw:whwlist)
                	whMAp.put(whw.WHID, whw.Hours);
            }
            
            TotalPerProject = 0;
            for(WorkingHourWrapper wh : SundayWH){
                TotalPerProject += whMAp.get(wh.WHID) ;
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : MondayWH){
                TotalPerProject += whMAp.get(wh.WHID);
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : TuesdayWH){
                TotalPerProject += whMAp.get(wh.WHID);
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : WednesdayWH){
                TotalPerProject += whMAp.get(wh.WHID);
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : ThursdayWH){
                TotalPerProject += whMAp.get(wh.WHID);
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : FridayWH){
                TotalPerProject += whMAp.get(wh.WHID);
                wh.Hours =  whMAp.get(wh.WHID);
            }
            for(WorkingHourWrapper wh : SaturdayWH){
                TotalPerProject += wh.Hours;
                wh.Hours =  whMAp.get(wh.WHID);
            }
        } 
        
        public void calcRowSpan(){
            RowSpan = Math.max(1,Math.max(SundayWH.Size(),Math.max(MondayWH.Size(),Math.max(TuesdayWH.Size(),Math.max(WednesdayWH.Size(),Math.max(ThursdayWH.Size(),Math.max(FridayWH.Size(),SaturdayWH.Size())))))));  
        }
        
        public void fillInWH(Date SundayD,Id UserId)
        {
            while(RowSpan > SundayWH.Size()){
                SundayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD,null,null));
                WHID++;
            }
            while(RowSpan > MondayWH.Size()){
                MondayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(1),null,null));
                WHID++;
            }
            while(RowSpan > TuesdayWH.Size()){
                TuesdayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(2),null,null));
                WHID++;
            }
            while(RowSpan > WednesdayWH.Size()){
                WednesdayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(3),null,null));
                WHID++;
            }
            while(RowSpan > ThursdayWH.Size()){
                ThursdayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(4),null,null));
                WHID++;
            }
            while(RowSpan > FridayWH.Size()){
                FridayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(5),null,null));
                WHID++;
            }
            while(RowSpan > SaturdayWH.Size()){
                SaturdayWH.add(new WorkingHourWrapper(isCompleted,null,this.Name,this.AccountId,null,WHID,ProjectId,0,null,true,false,false,null,null,SundayD.addDays(6),null,null));
                WHID++;
            }
		}   
}