public class MassWorkingHoursControllerLightning {
    @AuraEnabled
    public static Date SundayDate {get;set;}
     @AuraEnabled
    public static Date MondayDate {get;set;}
     @AuraEnabled
    public static Date TuesdayDate {get;set;}
     @AuraEnabled
    public static Date WednesdayDate {get;set;}
     @AuraEnabled
    public static Date ThursdayDate {get;set;}
     @AuraEnabled
    public static Date FridayDate {get;set;}
     @AuraEnabled
    public static Date SaturdayDate {get;set;}
    @AuraEnabled
    public static Work_hours__c WorkHours {get;set;}
    @AuraEnabled
    public static List<String> WeekDates {get;set;}
    @AuraEnabled
    public static List<ProjectWrapper> allProjects {get;set;}
    @AuraEnabled
    public static Map<String,String> allrelatedIssues {get;set;}
    @AuraEnabled 
    public static List<Double> Total {get;set;}
    
    @AuraEnabled
    public static Work_hours__c getWorkHours() {
      	WorkHours = new Work_hours__c();
        WorkHours.Date__c = Date.today();
        WorkHours.User__c = UserInfo.getUserId();
        return WorkHours;
  	}
    
    @AuraEnabled
    public static String getUserName() {
        User cUser = [Select Name From User Where Id =:UserInfo.getUserId() limit 1];
        return cUser.Name;
  	}

	 
    @AuraEnabled
    public static Boolean ifDaysoffProject(String ProjId) {
		map<string, string> ServiceWiseDayOffProjectMap = new map<string, string>();
        list<ServiceWiseDayOffProject__c> ServiceWiseDayOffProjectList = ServiceWiseDayOffProject__c.getAll().values();//[select name, ProjectId__c from ServiceWiseDayOffProject__c];
        if (ServiceWiseDayOffProjectList != null && ServiceWiseDayOffProjectList.size() > 0)
        {
            for (ServiceWiseDayOffProject__c sdf : ServiceWiseDayOffProjectList)
            {
                 ServiceWiseDayOffProjectMap.put(sdf.ProjectId__c, sdf.name);
            }
        }

        return ServiceWiseDayOffProjectMap.containsKey(ProjId);
       
  	}

    @AuraEnabled
    public static List<Double> getExpectedAndReportedapex(String cDate){
        Date d = Date.valueOf(cDate);
        Double expectedWorkingHours = getExpectedAmountOfHours(d);
		Double reportedWorkingHours = getReportedAmountOfHours(d);
        return new List<Double>{expectedWorkingHours,reportedWorkingHours };
    }

	@AuraEnabled
	public static Double getExpectedAmountOfHours(Date cDate){
		String month = formatDate(cDate,'MMMM');
		List<Working_Hour_Standard__c> expected = [Select Id,Amount_of_hours__c from Working_Hour_Standard__c where Name = :month];
		if(expected.Size() > 0)
		{
			return expected[0].Amount_of_hours__c;
		}
		return 0.0;
	}

	@AuraEnabled
	public static Double getReportedAmountOfHours(Date cDate){
		
		Id user_id = UserInfo.getUserId();
		Integer month = Integer.valueOf(formatDate(cDate,'M'));
		Integer year = Integer.valueOf(formatDate(cDate,'YYYY'));

		Double reported;
		
		if(month != null && (month >= 1 || month <= 12) )
		{
			List<Work_hours__c> workDays = [Select Hours__c,User__c,Date__C from Work_hours__c Where CALENDAR_MONTH(Date__c) = :month AND CALENDAR_YEAR(Date__c) = :year AND User__c = :user_id];
			reported = calcReportedAmountOfOurs(workDays);
		}
		//return as lower case
		return reported;

	}

	@AuraEnabled
	public static Double calcReportedAmountOfOurs(List<Work_hours__c> workDays){
		Double amountOfHours = 0;

		for(Work_hours__c workDay : workDays){
			amountOfHours += workDay.Hours__c;
		}

		return amountOfHours;
	}

	@AuraEnabled
	public static String formatDate(Date dateToFormat, String format){

		//convert date class to date time.
		Datetime dt = datetime.newInstance(dateToFormat.year(), dateToFormat.month(),dateToFormat.day());

		//return the format date.
		return dt.format(format);
	} 
    @AuraEnabled
     public static List<String> SetDaysOfWeekDates(String TodayDay){
        system.debug('TodayDay:'+ TodayDay);
        SundayDate = Date.valueOf(TodayDay);
        while(datetime.newInstance(SundayDate.Year(), SundayDate.Month(), SundayDate.Day()).format('EEEE') != 'Sunday'){
            SundayDate = SundayDate.addDays(-1);    
        }
        MondayDate = SundayDate.addDays(1);
        TuesdayDate = SundayDate.addDays(2);
        WednesdayDate = SundayDate.addDays(3);
        ThursdayDate = SundayDate.addDays(4);
        FridayDate = SundayDate.addDays(5);
        SaturdayDate = SundayDate.addDays(6);
        WeekDates = new List<String>{String.valueOf(SundayDate),String.valueOf(MondayDate),String.valueOf(TuesdayDate),String.valueOf(WednesdayDate),String.valueOf(ThursdayDate), String.valueOf(FridayDate),String.valueOf(SaturdayDate)};
        system.debug('WeekDates: '+ WeekDates);
        return WeekDates;
     }
    @AuraEnabled
    public static List<ProjectWrapper> setProjects(String Date4WH)
    {
        WeekDates = SetDaysOfWeekDates(Date4WH);
        allProjects = new List<ProjectWrapper>();
        List<Work_hours__c> allWH = [SELECT Internal_Comments__c, Customer_Description__c, Project_name__r.SFDC_Project_Status__c,Issue_Number__c,Hours__c,Id,Description__c,Billable__c,Vacation__c,Sickness__c,Travel_way__c,Date__c,Account__c,Project_name__c,Project_name__r.Name,Account__r.Name, AbsenceReason__c FROM Work_hours__c WHERE Date__c >= :date.valueOf(WeekDates[0])AND Date__c <= :date.valueOf(WeekDates[6]) AND User__c = :UserInfo.getUserId()];
        system.debug('allWH'+allWH);
        Map<Id, Id> projects = new Map<Id, Id>(); 
        for(Work_hours__c WH : allWH){
            if(!projects.containsKey(WH.Project_name__c))
            {
                allProjects.add(new ProjectWrapper((WH.Project_name__r.SFDC_Project_Status__c == 'Completed'),allWH,WH.Account__r.Name,WH.Project_name__r.Name,WH.Project_name__c,date.valueOf(WeekDates[0]),UserInfo.getUserId(),WH.Account__c,false));
                projects.put(WH.Project_name__c,WH.Project_name__c);
            }
        }  
        system.debug('allProjects:'+allProjects);
        SortProjectByAccount();
        return allProjects;
    }
 	@AuraEnabled
    //set colors to the lines in table
    public static void SortProjectByAccount()
    {
        for(Integer i = 0; i < allProjects.size() - 1; i++)
        {
            for(Integer j = 0; j < allProjects.size() - 1; j++)
            {
                if(allProjects[j].AccountName > allProjects[j+1].AccountName)
                {
                    ProjectWrapper temp = allProjects[j];
                    allProjects[j] = allProjects[j+1];
                    allProjects[j+1] = temp;
                }
            }
        }   
        for(Integer i = 0; i < allProjects.size(); i++){
            if(Math.mod(i,2) == 0){
                allProjects[i].color = '#eee';
            }
            else{
                allProjects[i].color = '#fff';
            }
        }
    }
    
    @AuraEnabled
    public static List<Decimal> calcTotalPerDay(String jsonallProj){
		List<ProjectWrapper> allproj= (List<ProjectWrapper>)JSON.deserialize(jsonallProj, List<ProjectWrapper>.class );
        Total = new List<Decimal>();
        for(Integer i = 0; i < 8; i++)
            Total.add(0);
        Decimal weekTotal = 0;
        for(ProjectWrapper p : allProj){
            for(WorkingHourWrapper WH : p.SundayWH){
                Total[0] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.MondayWH){
                Total[1] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.TuesdayWH){
                Total[2] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.WednesdayWH){
                Total[3] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.ThursdayWH){
                Total[4] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.FridayWH){
                Total[5] += WH.Hours;
                weekTotal += WH.Hours;
            }
            for(WorkingHourWrapper WH : p.SaturdayWH){
                Total[6] += WH.Hours;
                weekTotal += WH.Hours;
            }
            Total[7] = weekTotal;
        }
        system.debug('Total: '+ Total);
        return Total;
    }
    @AuraEnabled
    //calculate total per Project after hours were changed by user
    public static List<ProjectWrapper>  reCalc(String jsonallProjects){
        List<ProjectWrapper> allproj = (List<ProjectWrapper>)JSON.deserialize(jsonallProjects, List<ProjectWrapper>.class );
        for(ProjectWrapper p : allproj)
            p.calcTotalPerProject(); 
        return allproj;
    }
    
    @AuraEnabled
    public static List<ProjectWrapper> SaveDataapex(String jsonallProjects,String Date4WH){
		allProjects = (List<ProjectWrapper>)JSON.deserialize(jsonallProjects, List<ProjectWrapper>.class);
        //Date thisweekdate = allProjects[0].firstLineAllWeekWH[0].WHDate;
        List<Work_hours__c> upsertWH = new List<Work_hours__c>();
        List<WorkingHourWrapper> allWH = new List<WorkingHourWrapper>();
        for(ProjectWrapper p : allProjects){
            for(WorkingHourWrapper whw:p.firstLineAllWeekWH){
                allWH.add(whw);
            }
            for(List<WorkingHourWrapper> whwlist:p.restAllWeekWH){
                for(WorkingHourWrapper whw:whwlist)
                	allWH.add(whw);
            }
         }
         List<Id> updateWHID = new List<Id>();
         List<Id> deleteWHID = new List<Id>();
         for(WorkingHourWrapper wh : allWH){ 
			system.debug( wh.WorkingHourId );
             if((wh.Hours != 0) && (wh.WorkingHourId != null)){
                 updateWHID.add(wh.WorkingHourId);
             }
             else if((wh.Hours == null || wh.Hours == 0) && wh.WorkingHourId != null ){
                 deleteWHID.add(wh.WorkingHourId);
             }
        }
        system.debug('update: '+ updateWHID);
        system.debug('delete: '+ deleteWHID);
        Map<Id,Work_hours__c> allWH2Update = new Map<Id,Work_hours__c>([SELECT Internal_Comments__c,Customer_Description__c,Id,Project_name__c,Hours__c,Description__c,Billable__c,Vacation__c,Sickness__c,Travel_way__c,Date__c,Issue_Number__c FROM Work_hours__c WHERE Id IN :updateWHID AND Project_name__r.SFDC_Project_Status__c != 'Completed']);
        List<Work_hours__c> deleteWH = [SELECT Id FROM Work_hours__c WHERE Id IN :deleteWHID];

        for(WorkingHourWrapper wh : allWH){     
            	//if update
                if((wh.Hours != 0) && (wh.WorkingHourId != null)){
					allWH2Update.get(wh.WorkingHourId).Hours__c = wh.Hours;
					allWH2Update.get(wh.WorkingHourId).Description__c = wh.Description;
					allWH2Update.get(wh.WorkingHourId).Billable__c = wh.Billable;
					if(wh.Travelway != '--Select--')
						allWH2Update.get(wh.WorkingHourId).Travel_way__c = wh.Travelway;
					if(wh.AbsenceReason != '--Select--')
						allWH2Update.get(wh.WorkingHourId).AbsenceReason__c = wh.AbsenceReason;
					
						allWH2Update.get(wh.WorkingHourId).Issue_Number__c = wh.IssueNumber;
                    allWH2Update.get(wh.WorkingHourId).Internal_Comments__c = wh.InternalComments;
					if(wh.CustomerDescription != '--Select--')
						allWH2Update.get(wh.WorkingHourId).Customer_Description__c = wh.CustomerDescription;
                    system.debug('Update:'+ wh.WorkingHourId);
                }//if create
                else if((wh.Hours != 0) && (wh.WorkingHourId == null)){
                    Work_hours__c newWH = new Work_hours__c();
                    newWH.Date__c = wh.WHDate;
                    newWH.Project_name__c = wh.Project;
                    newWH.Hours__c = wh.Hours;
                    newWH.Description__c = wh.Description;
                    newWH.Billable__c = wh.Billable;
					if(wh.Travelway != '--Select--')
						newWH.Travel_way__c = wh.TravelWay;
					if(wh.AbsenceReason != '--Select--')
						newWH.AbsenceReason__c = wh.AbsenceReason;
                    newWH.Account__c = wh.AccountId;
                    newWH.Issue_Number__c = wh.IssueNumber;
                    newWH.Internal_Comments__c = wh.InternalComments;
					if(wh.CustomerDescription != '--Select--')
						newWH.Customer_Description__c = wh.CustomerDescription;
                    newWH.User__c = UserInfo.getUserId();
                    upsertWH.add(newWH);
                }
            }
        	system.debug('upsertWH: '+ upsertWH);
        	system.debug('deleteWH: '+ deleteWH);
            upsertWH.addAll(allWH2Update.values());
            try{
                if(!upsertWH.isEmpty())
                    upsert upsertWH;
                if(!deleteWH.isEmpty())
                    delete deleteWH;    
           		return setProjects(Date4WH);
            }
            catch(DmlException e){
              system.debug('Error'+e.getCause());
            } 

        return null;
    }
    @AuraEnabled
    public static List<ProjectWrapper> checkValidations(String jsonallProjects){
        allProjects = new List<ProjectWrapper>();
		allProjects = (List<ProjectWrapper>)JSON.deserialize(jsonallProjects, List<ProjectWrapper>.class);
        Boolean noError = true;
        List<WorkingHourWrapper> allWH = new List<WorkingHourWrapper>();
        for(ProjectWrapper p : allProjects){
			
            p.Error = false;
        	for(WorkingHourWrapper wh : p.firstLineAllWeekWH){
				System.debug(wh);
                if(!wh.Validate(ifDaysoffProject(wh.Project))){
            		noError = false;
                    p.Error = true;
                }
        	}
            for(List<WorkingHourWrapper> issie : p.restAllWeekWH){
                for(WorkingHourWrapper wh:issie) 
                    if(!wh.Validate(ifDaysoffProject(wh.Project))){
            			noError = false;   
                        p.Error = true;
                }
        	}       
        }
        system.debug('noError = '+ noError);
        return allProjects;
	}
    
    @AuraEnabled
    public static Map<String,String> GetRelatedIssues(String ProjectId,String OnlyOpen,String thisIssue)
    { 	
       system.debug('thisIssue: '+ thisIssue);
        Boolean Open = Boolean.valueOf(OnlyOpen);
        allrelatedIssues = new Map<String,String>();
        List<SFDC_Issue__c> issues = new List<SFDC_Issue__c> ();
        List<String> closedstatuses = new List<String>{'3 - Closed', '5 - Cancelled'};
        if(Open)
        	issues = [SELECT Id,Name, SFDC_Issue_Name__c FROM SFDC_Issue__c WHERE (SFDC_Issue_Status__c NOT in:closedstatuses OR Id =:thisIssue) AND SFDC_Project__c = :ProjectId order by Name desc];
        else
        	issues = [SELECT Id,Name, SFDC_Issue_Name__c FROM SFDC_Issue__c WHERE SFDC_Project__c = :ProjectId order by Name desc];
        allrelatedIssues.put('--Select--','');
        for(SFDC_Issue__c i : issues){
          allrelatedIssues.put(i.Name + ' - ' + i.SFDC_Issue_Name__c,i.Id);
        }
        system.debug('issues:'+ allrelatedIssues);
        return allrelatedIssues;
    }
    @AuraEnabled
    public static Map<String,String> GetRelatedProjects(String AccountId)
    { 
        Map<String,String> allrelatedProjects = new Map<String,String>();
        List<SFDC_Project__c> Projects = new List<SFDC_Project__c> (); 
        Projects = [SELECT Id,Name, Account__c, Account__r.Name FROM SFDC_Project__c WHERE Account__c = :AccountId AND SFDC_Project_Status__c != 'Completed'];
        allrelatedProjects.put('--Select--','');
        for(SFDC_Project__c i : Projects)
          allrelatedProjects.put(i.Name,i.Id);
        return allrelatedProjects;
    }
    @AuraEnabled
    public static Map<String,String> GetallAccounts()
    { 
        Map<String,String> allAccounts= new Map<String,String>();
        List<Account> allAccount = [SELECT Id,Name FROM Account Where Account_Status__c != 'Not Active' ORDER BY Name];
        allAccounts.put('--Select--','');
        for(Account i : allAccount)
          allAccounts.put(i.Name,i.Id);
        return allAccounts;
    }
    @AuraEnabled
    public static List<ProjectWrapper> addProject(String jsonallProjects,String ProjectToAdd,String Date4WH ){
		allProjects = (List<ProjectWrapper>)JSON.deserialize(jsonallProjects, List<ProjectWrapper>.class);
        SetDaysOfWeekDates(Date4WH);
       ProjectWrapper Project2Add;
            for(ProjectWrapper p : allProjects){
                if(p.ProjectId == ProjectToAdd){
                    Project2Add = p;
                    Project2Add.SundayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate,null,null));
                    Project2Add.WHID++;
                    Project2Add.MondayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(1),null,null));
                    Project2Add.WHID++;
                    Project2Add.TuesdayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(2),null,null));
                    Project2Add.WHID++;
                    Project2Add.WednesdayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(3),null,null));
                    Project2Add.WHID++;
                    Project2Add.ThursdayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(4),null,null));
                    Project2Add.WHID++;
                    Project2Add.FridayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(5),null,null));
                    Project2Add.WHID++;
                    Project2Add.SaturdayWH.add(new WorkingHourWrapper(Project2Add.isCompleted,null,Project2Add.Name,Project2Add.AccountId,null,Project2Add.WHID,ProjectToAdd,0,null,true,false,false,null,null,SundayDate.addDays(6),null,null));
                    Project2Add.WHID++;
                    Project2Add.calcRowSpan();
                    Project2Add.setWeekWH();    
                }
            }
            if(Project2Add == null){
                SFDC_Project__c p = [SELECT Name, Account__r.Name, SFDC_Project_Status__c, Account__c FROM SFDC_Project__c WHERE Id = :ProjectToAdd];
                List<Work_hours__c> emptyWH = new List<Work_hours__c>();
                Project2Add = new ProjectWrapper((p.SFDC_Project_Status__c == 'Completed'),emptyWH,p.Account__r.Name,p.Name,ProjectToAdd,SundayDate,UserInfo.getUserId() ,p.Account__c,false);
                allProjects.add(Project2Add);
                SortProjectByAccount();
            } 
        system.debug('Projects after: ' + allProjects);
        return allProjects;        
    }
  
    //builds dinamic picklists
    @AuraEnabled
    public static List<String> GetPickListValue(string objectType,string picklistName,boolean hasNoneValue)
        {
            Map<String, Schema.SObjectType> m = Schema.getGlobalDescribe() ;    
            List<String> op = new List<String>();
            Schema.SObjectType s = m.get(objectType) ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            if(r==null){
                return op;
            }
            Map<String, Schema.SObjectField> fields = r.fields.getMap(); 
            Schema.SObjectField f = fields.get(picklistName);   
            if(f==null){
                return op;
            }
            Schema.DescribeFieldResult r2 = f.getDescribe() ;
            if(r2==null)
                return op;
            Schema.PicklistEntry [] Commpref = r2.getPickListValues();
            if(hasNoneValue)
                op.add('--Select--');
            for(Schema.PicklistEntry val : Commpref) 
            {
              op.add(val.getValue());
            }
            return op;
        }
    @AuraEnabled
    public static List<ProjectWrapper> GetProjectsFromLastWeekapex(String jsonallProjects,String Date4WH ){
        allProjects = (List<ProjectWrapper>)JSON.deserialize(jsonallProjects, List<ProjectWrapper>.class);
        Map<Id,Id> projects = new Map<Id,Id>();
        for(ProjectWrapper p : allProjects){
            projects.put(p.ProjectId,p.ProjectId);
        }
		SetDaysOfWeekDates(Date4WH);
        List<Work_hours__c> lastWeekWH = [SELECT Internal_Comments__c, Customer_Description__c,Issue_Number__c,Hours__c,Id,Description__c,Billable__c,Vacation__c,Sickness__c,Travel_way__c,Date__c,Account__c,Project_name__c,Project_name__r.Name,Account__r.Name FROM Work_hours__c WHERE Date__c >= :SundayDate.addDays(-7) AND Date__c <= :SundayDate.addDays(-1) AND User__c = :UserInfo.getUserId() AND Project_name__r.SFDC_Project_Status__c != 'Completed'];
        for(Work_hours__c WH : lastWeekWH){
            if(!projects.containsKey(WH.Project_name__c)){
                List<Work_hours__c> emptyWH = new List<Work_hours__c>();
                allProjects.add(new ProjectWrapper(false,emptyWH,WH.Account__r.Name,WH.Project_name__r.Name,WH.Project_name__c,SundayDate,UserInfo.getUserId(),WH.Account__c,false));
                projects.put(WH.Project_name__c,WH.Project_name__c);
            }
        }   
        SortProjectByAccount();
        return allProjects;
    }
}