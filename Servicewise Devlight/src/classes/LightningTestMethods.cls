/******************************************************************************* 
Name              : StartStopIssue
Description       : controller of StartStop lightning component
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny 			  01/02/2017				  Maor					[SW-22723]
*******************************************************************************/
@isTest
public class LightningTestMethods {
@isTest
   public static void myUnitTest() {
       SFDC_Project__c project = new SFDC_Project__c(QA_is_required_for_all_project_issues__c = 'No');
       insert project;
       SFDC_Issue__c issue = new SFDC_Issue__c(SFDC_Project__c = project.Id,Timer_Date__c = system.today(),SFDC_Issue_Status__c='2 - In Progress' , Timer_User__c = UserInfo.getUserId());
       insert issue;
       system.debug('issue before query: '+issue);
       issue.Timer_Date__c = system.today();
       issue.Start_Time__c=DateTime.now();
       issue.Timer_Hours__c=5;
       update issue;
       issue = [Select SFDC_Project__c,Timer_Date__c, Start_Time__c, Timer_Hours__c, SFDC_Issue_Status__c , Timer_User__c ,Current_Timer_Hours__c From SFDC_Issue__c Where Id = :issue.Id];
       system.debug('issue after query: '+issue);
       SFDC_Issue__c issue1 = issue.clone();
       insert issue1;
       issue1.Timer_Date__c = system.today().addDays(-2);
       issue1.Start_Time__c = DateTime.now().addDays(-2);
       issue1.Timer_Hours__c = null;
       update issue1;
       
       system.debug('issue1 befor query: '+issue1);
       issue1 = [Select SFDC_Project__c,Timer_Date__c,Start_Time__c,Timer_Hours__c,SFDC_Issue_Status__c , Timer_User__c ,Current_Timer_Hours__c From SFDC_Issue__c Where Id = :issue1.Id];
       system.debug('issue1 after: '+issue1);
       StartStopIssue.updateIssue(issue.Id);
       StartStopIssue.updateIssue(issue.Id);
       StartStopIssue.updateIssue(issue1.Id);
    
       
    }
}