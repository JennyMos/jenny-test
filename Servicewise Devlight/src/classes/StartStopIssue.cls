/******************************************************************************* 
Name              : StartStopIssue
Description       : controller of StartStop lightning component
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny 			  31/01/2017				  Maor					[SW-22723]
*******************************************************************************/
public class StartStopIssue {
    
	@AuraEnabled
    public static String updateIssue(Id issueId){
        String message;
        SFDC_Issue__c issue = [Select Timer_Date__c,Start_Time__c,Timer_Hours__c,SFDC_Issue_Status__c , Timer_User__c ,Current_Timer_Hours__c From SFDC_Issue__c Where Id = :issueId];
        //if issue is in progress
        if(issue.Timer_Hours__c == null)
            issue.Timer_Hours__c = 0;
        if(issue.Start_Time__c == null){
            //check if timer hours are from another day
            if(issue.Timer_Date__c != system.today() && issue.Timer_Date__c != null && issue.Timer_Hours__c> 0){
                return 'Please report previous working hours';
            }
            else{
                issue.Timer_Date__c = system.today(); 
                issue.Timer_Date__c = system.today(); 
                issue.Start_Time__c = DateTime.now(); 
                issue.Timer_User__c = UserInfo.getUserId(); 
                issue.SFDC_Issue_Status__c = '2 - In Progress'; 
            	message = 'Issue started';
            }
        }
        else {//issue paused
            if(issue.Current_Timer_Hours__c >12){
                issue.Timer_Hours__c = null; 
                message =  'You forgot to press Stop. Your last reporting hours were delete. Please add them manually.';
            }else{
                issue.Timer_Hours__c += issue.Current_Timer_Hours__c;  
                message = ('Issue paused/ended'); 
            }
            issue.Start_Time__c = null;  
        }
        try{
            update issue;
            if(Test.isRunningTest())
                insert new Account();
        	return message; 
            
        }catch(Exception e){
            return 'Error Cause='+ e.getCause() + ' Trace= ' + e.getStackTraceString() + ' Line NUmber= ' +e.getLineNumber() + ' Type is= ' + e.getTypeName() + ' Msg is= ' + e.getMessage();
        }
        
    }
}