/************************************************************************************** 
Name              : MassWorkingHoursControllerLightning_Test 
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny	              25/04/2017               Dana                  [SW-23982]
****************************************************************************************/
@isTest
public class MassWorkingHoursControllerLightning_Test {
	static testMethod void Controller_Test(){ 
		Account acc = new Account(Name = 'acc name', MigrationTest__c = '345345');
		insert acc;
		SFDC_Project__c proj =  new SFDC_Project__c(Name='Test Project',Account__c = acc.Id,QA_is_required_for_all_project_issues__c = 'No');
		insert proj;
        SFDC_Project__c proj2 = proj.clone();
        insert proj2;
		SFDC_Issue__c issue = new SFDC_Issue__c(SFDC_Issue_Name__c='Tests',SFDC_Project__c = proj.Id);
		insert issue;
		Work_hours__c wh = new Work_hours__c(Project_name__c = proj.Id,Date__c = system.today(),Description__c ='',Account__c = acc.Id,Issue_Number__c = issue.Id,Hours__c=5,User__c= system.UserInfo.getUserId());
		insert wh;
        Work_hours__c wh2 = wh.clone();
        wh2.Description__c ='Returns the preferred theme for the current user. Use getUiThemeDisplayed to determine the theme actually displayed to the current user.Signature public static String getUiTheme()';
		wh2.Issue_Number__c = null;
        insert wh2;
		
        Test.startTest();
		MassWorkingHoursControllerLightning.getWorkHours();
		MassWorkingHoursControllerLightning.getUserName();
		MassWorkingHoursControllerLightning.ifDaysoffProject(proj.Id);
		List<String> picklistv = MassWorkingHoursControllerLightning.GetPickListValue('Work_hours__c','AbsenceReason__c',true);
		Map<String,String> accmap = MassWorkingHoursControllerLightning.GetallAccounts();
		MassWorkingHoursControllerLightning.getExpectedAndReportedapex(String.valueOf(system.today()));
		List<ProjectWrapper> projlist = MassWorkingHoursControllerLightning.setProjects(String.valueOf(system.today()));
		String projectstring = JSON.serialize(projlist);
		
		projlist = MassWorkingHoursControllerLightning.addProject(projectstring,proj.Id,String.valueOf(system.today()));
        SFDC_Project__c proj3 = proj.clone();
        insert proj3;
        Work_hours__c wh3 = wh2.clone();
        wh3.Date__c= system.today().addDays(-2);
        wh3.Project_name__c = proj3.Id;
        insert wh3;

        projlist = MassWorkingHoursControllerLightning.addProject(projectstring,proj3.Id,String.valueOf(system.today()));
		projectstring = JSON.serialize(projlist);
		projlist = MassWorkingHoursControllerLightning.reCalc(projectstring);
		projectstring = JSON.serialize(projlist);
		projlist = MassWorkingHoursControllerLightning.checkValidations(projectstring);
		projectstring = JSON.serialize(projlist);
		System.debug(projectstring);
		projlist = MassWorkingHoursControllerLightning.SaveDataapex(projectstring,String.valueOf(system.today()));

		Map<String,String> mapp = MassWorkingHoursControllerLightning.GetRelatedIssues(proj.Id,'true','');
		Map<String,String> mapp2 = MassWorkingHoursControllerLightning.GetRelatedProjects(acc.Id);
		List<Decimal> dec = MassWorkingHoursControllerLightning.calcTotalPerDay(projectstring);
		projectstring = JSON.serialize(projlist);
		projlist = MassWorkingHoursControllerLightning.GetProjectsFromLastWeekapex(projectstring,String.valueOf(system.today()));
		projlist[0].firstLineAllWeekWH.add(new WorkingHourWrapper(false,issue.Id,'Test Project',acc.Id,null,2,proj2.Id,5,null,true,false,false,null,null,MassWorkingHoursControllerLightning.SundayDate,null,null));
		projectstring = JSON.serialize(projlist);
		projlist = MassWorkingHoursControllerLightning.SaveDataapex(projectstring,String.valueOf(system.today()));
		
		Test.stopTest();
		
	}


}