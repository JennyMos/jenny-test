/************************************************************************************** 
Name              : UpdateAccountSiteManagerBatch
Description       : Run by scheduler 2 a day, updates Site_Manager_Text__c of Invoices and POs
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny               12/06/2017      			    Eyal Morad         		[SW-25299]
****************************************************************************************/
global class UpdateAccountSiteManagerBatch implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext BC){

		String query = 'Select Site_Manager_Changed__c From Account Where Site_Manager_Changed__c = true';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Account> scope){
		Set<Id> AccountIDs = new Set<Id>();
		List<Invoice_c__c> Invs = new  List<Invoice_c__c>();
		List<Purchase_Order__c> POrders = new  List<Purchase_Order__c>();
		for(Account s : scope){
			AccountIDs.add(s.Id); 
			s.Site_Manager_Changed__c = false;
		}
	    Invs = [Select Account_Name__r.Site_Manager__r.Name From Invoice_c__c Where Account_Name__c in:AccountIDs];
		POrders = [Select Account__r.Site_Manager__r.Name From Purchase_Order__c Where Account__c in:AccountIDs];
		for (Invoice_c__c inv:Invs)
			inv.Site_Manager_Text__c = inv.Account_Name__r.Site_Manager__r.Name;
		for (Purchase_Order__c po:POrders)
			po.Site_Manager_Text__c = po.Account__r.Site_Manager__r.Name;
		try{
			update Invs;
			update POrders;
			update scope;
		}catch(Exception ex){
			System.debug(ex.getCause());
		}
   }

   global void finish(Database.BatchableContext BC){

   }

	
}