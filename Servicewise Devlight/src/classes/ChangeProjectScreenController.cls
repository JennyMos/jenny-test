public with sharing class ChangeProjectScreenController {

	public List<SelectOption> allProjects {get;set;}
	public Id ProjectId {get;set;}
	public class MyException extends Exception {} 
	public Work_hours__c oldWH;
	public Work_hours__c newWH {get;set;}
	
	public ChangeProjectScreenController(){
		String WHID = Apexpages.currentPage().getParameters().get('Id');
		try{
			oldWH = [SELECT Issue_Number__c,Vacation__c,User__c,Travel_way__c,Sickness__c,Opportunity__c,OldID__c,Hours__c,Id,Account__c,Billable__c,Date__c,Description__c FROM Work_hours__c WHERE Id = :WHID];
		}
		catch(Exception e){}		
		allProjects = new List<SelectOption>();
		List<SFDC_Project__c> projects = [SELECT Id,Name FROM SFDC_Project__c WHERE Account__c = :oldWH.Account__c AND SFDC_Project_Status__c != 'Completed'];
		for(SFDC_Project__c p : projects){
			SelectOption NewSO = New SelectOption(p.Id,p.Name);
            allProjects.add(NewSO);
		}
	}
	
	public Pagereference save(){
		try{
			newWH = oldWH.Clone(false,true,true,true);
			newWH.Project_name__c = ProjectId;
			insert newWH;
			List<Attachment> allAttachments = [SELECT ContentType,ConnectionSentId,ConnectionReceivedId,BodyLength,OwnerId,Name,IsPrivate,Description,Body,Id,ParentId FROM Attachment WHERE ParentId = :oldWH.Id];
			List<Attachment> newAttachments = new List<Attachment>();
			for(Attachment a : allAttachments){
				Attachment newa = a.Clone(false,true,true,true);
				newa.ParentId = newWH.Id;
				newAttachments.add(newa);
			}
			insert newAttachments;
			
			List<Note> allNotes = [SELECT Title,IsPrivate,OwnerId,Body,Id,ParentId FROM Note WHERE ParentId = :oldWH.Id];
			List<Note> newNotes = new List<Note>();
			for(Note a : allNotes){
				Note newa = a.Clone(false,true,true,true);
				newa.ParentId = newWH.Id;
				newNotes.add(newa);
			}
			insert newNotes;		
			
			delete oldWH;
			
		}
		catch(Exception e){
        	ApexPages.addMessages(new MyException(e.getMessage()));
        }
        return null;
	}
	
	static testMethod void ChangeProjectScreenController_Test() {
		Account a = new Account(Name='Test');
		insert a;
		SFDC_Project__c project = new SFDC_Project__c(Name = 'test',Account__c=a.Id);
		insert project;
		Work_hours__c WH = new Work_hours__c(Project_name__c = project.Id,Hours__c=8,Description__c='abc',Account__c=a.Id);
		insert WH;
		Attachment att = new Attachment(Name='test',ParentId=WH.Id);
		Blob b = Blob.valueof('Hellow World');
		att.Body = b;
		insert att;
		Note no = new Note(Title='test',ParentId=WH.Id);
		insert no;
		Apexpages.currentPage().getParameters().put('Id',String.ValueOf(WH.Id));
		ChangeProjectScreenController controller = new ChangeProjectScreenController();
		controller.ProjectId = project.Id;
		controller.save();
		
	}
}