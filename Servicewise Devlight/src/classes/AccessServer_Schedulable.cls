/****************************************************************************************
    Name              : AccessServer_Schedulable
    Description       : Scheduled task to remove ip from Access Settings
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue  
    ----------------------------------------------------------------------------------------
    1. Gal (created)            24/03/2015               Dana             [SW-10678]
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
global class AccessServer_Schedulable implements Schedulable{
    
    private Id loginDetailsId;
    private String ipAddress;
    
    public AccessServer_Schedulable(Id loginDetailsId, String ipAddress){
        this.loginDetailsId = loginDetailsId;
        this.ipAddress = ipAddress;
    }
    
    global void execute(SchedulableContext sc){
        AccessServer.removeIPfromNetworkAccessFuture(loginDetailsId, ipAddress);
    }
}