/******************************************************************************* 
Name              : ReportRecurrentJobWHCtrl
Description       : 
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mendy               07/01/2015                  Yaara                 [SW-11747]
2. Jenny               11/07/2017                  Yaara                 [SW-25619]
*******************************************************************************/
public with sharing class ReportRecurrentJobWHCtrl {

	public Work_hours__c wh {get; set;}
	public string projectId {get; set;}
	public string dateStr {get; set;}
	public boolean allWeek {get; set;}
	public boolean isError {get; set;}
	public boolean isDayOff {get; set;}
	public SFDC_Project__c proj {get; set;}
	
	public ReportRecurrentJobWHCtrl()
	{
		projectId = Apexpages.currentPage().getParameters().get('projectId');
		dateStr = Apexpages.currentPage().getParameters().get('date');
		proj = [select id, name, Account__c from SFDC_Project__c where id = :projectId];
		wh = new Work_hours__c();
		CheckIfIsDayOffProj();
	}
	
	private void CheckIfIsDayOffProj()
	{
		system.debug('projectId = ' + projectId);
		isDayOff = false;
        for(ServiceWiseDayOffProject__c dop : ServiceWiseDayOffProject__c.getAll().values())
        {
			system.debug('dop.ProjectId__c = ' + dop.ProjectId__c);
        	if (projectId.contains(dop.ProjectId__c))
				isDayOff = true;
        }
		  
	}
	
	public void SetDays()
	{
		if (allWeek)
		{
			//string dateStr = Apexpages.currentPage().getParameters().get('date');
			system.debug('dateStr = ' + dateStr);
			system.debug('dateStr = ' + dateStr.substring(0,2));
			system.debug('dateStr = ' + dateStr.substring(3,5));
			system.debug('dateStr = ' + dateStr.substring(6));
			Datetime dt = Datetime.newInstance(integer.valueof(dateStr.substring(6)),integer.valueof(dateStr.substring(3,5)),integer.valueof(dateStr.substring(0,2)),  3,3,3);
			system.debug('dt = ' + dt);
			wh.From_Date__c = dt.dateGmt().toStartOfWeek();
			wh.To_Date__c = wh.From_Date__c.AddDays(4);
		}
	}
	
	public void Save()
	{
		if (Validation())
		{
			isError = false;
			list<Work_hours__c> whToInsert = new list<Work_hours__c>();
			for (Date a = wh.From_Date__c; a<= wh.To_Date__c; a = a.addDays(1))
			{
				Work_hours__c newWH = new Work_hours__c(Project_name__c = projectId, Account__c = proj.Account__c);
				newWH.Date__c = a;
				newWH.Hours__c = wh.Hours__c;
				newWH.Description__c = wh.Description__c;
				newWH.Travel_way__c = wh.Travel_way__c;
				newWH.AbsenceReason__c = wh.AbsenceReason__c;
				newWH.User__c = UserInfo.getUserId();
				newWH.Customer_Description__c = wh.Customer_Description__c;
				whToInsert.add(newWH);
			}
			system.debug('whToInsert == ' + whToInsert);
			if (whToInsert.size() > 0)
			{
				try
				{
					insert whToInsert;
				}
				catch (exception e)
				{
					isError = true;
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
				}
			}
		}
		else
			isError = true;
	}
	
	private boolean Validation()
	{
		boolean ret = true;
		if (wh.Hours__c == null)
		{
			wh.Hours__c.addError('You must enter a value');
			ret = false;
		}
		if (wh.From_Date__c == null)
		{
			wh.From_Date__c.addError('You must enter a value');
			ret = false;
		}
		if (wh.To_Date__c == null)
		{
			wh.To_Date__c.addError('You must enter a value');
			ret = false;
		}
		if (wh.Customer_Description__c == null && !isDayOff)
		{
			wh.Customer_Description__c.addError('You must enter a value');
			ret = false;
		}
		if (wh.To_Date__c < wh.From_Date__c)
		{
			wh.From_Date__c.addError('To Date must be greater than From Date');
			wh.To_Date__c.addError('To Date must be greater than From Date');
			ret = false;
		}
		return ret;
	}
}