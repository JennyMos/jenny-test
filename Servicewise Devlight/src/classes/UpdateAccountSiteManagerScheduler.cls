/************************************************************************************** 
Name              : UpdateAccountSiteManagerScheduler
Description       : Run by scheduler 2 a day
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny               12/06/2017      			    Eyal Morad         		[SW-25299]
****************************************************************************************/
global class UpdateAccountSiteManagerScheduler implements Schedulable {
  
   global void execute(SchedulableContext SC) {
      UpdateAccountSiteManagerBatch b = new UpdateAccountSiteManagerBatch(); 
      database.executebatch(b,50); 
   }

}