/******************************************************************************* 
Name              : TriggerSettings
Description      : Settings for enabling or disabling triggers
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Hanit              25/04/2016                                         
*******************************************************************************/
global class TriggerSettings {
   
  public static Map<String, Set<String>> disabledTriggers;
  public static Set<String> disabledTriggersByTestOnWholeTriggerName = new Set<String>();
  
  public static Boolean IsTriggerActive( String triggerName , String functionName){
    
    Boolean condition = false; 
     
    if( disabledTriggers == null ){  
      disabledTriggers = new Map<String, Set<String>>();
      for(Triggers_To_Turn_Off__c oneTrigger : Triggers_To_Turn_Off__c.getAll().values()) {
        if(disabledTriggers.containskey(oneTrigger.Trigger_API_Name__c.ToLowerCase())){
      if(checkUserProfile(oneTrigger)){
        disabledTriggers.get(oneTrigger.Trigger_API_Name__c.ToLowerCase()).add(oneTrigger.Function_Name__c.ToLowerCase());
      }
        }else{  
            disabledTriggers.put(oneTrigger.Trigger_API_Name__c.ToLowerCase() , new Set<String>());
      if(checkUserProfile(oneTrigger)){
        disabledTriggers.get(oneTrigger.Trigger_API_Name__c.ToLowerCase()).add(oneTrigger.Function_Name__c.ToLowerCase());
      }
          }
      } 
    }
    
    condition = disabledTriggers.containskey(triggerName.toLowerCase());
    if(condition) condition &= disabledTriggers.get(triggerName.toLowerCase() ).contains(functionName.toLowerCase());
    
    return !disabledTriggersByTestOnWholeTriggerName.contains(triggerName) && ( (!condition) || Test.isRunningTest() );
  }
  
  //this function checks if the users' profile is disabled, if it is added to the disabledTriggers, if the profile field is null, the function is added to disabledTriggers
  private static boolean checkUserProfile(Triggers_To_Turn_Off__c oneTrigger){
    String userProfile = userinfo.getProfileId();
    System.debug('userProfile==='+userProfile);
    if(oneTrigger.Profile_ByPass_Triggers__c != null){
      System.debug('oneTrigger.Profile_ByPass_Triggers__c==='+oneTrigger.Profile_ByPass_Triggers__c);
      if(oneTrigger.Profile_ByPass_Triggers__c == userProfile)
        return true;
      else return false;
    }
    return true;
  }
}