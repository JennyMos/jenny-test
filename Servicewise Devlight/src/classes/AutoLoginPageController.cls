public with sharing class AutoLoginPageController {
    
    public Login_Details__c ld {get; set;}
    public class MyException extends Exception {}
    public void login(){
        try{
        String LoginDetailsId = Apexpages.currentPage().getParameters().get('Id');
        ld = [SELECT Username__c,Password__c,Environment__c FROM Login_Details__c WHERE Id = :LoginDetailsId];
        
        //We can't use server-side login because login requires a cookie creation
        
        /*String env = '';
        
        env='login';
        if(ld.Environment__c == 'Salesforce Sandbox')
            env='test';

        String url2Redirect = 'https://'+env+'.salesforce.com/?un=' + ld.Username__c + '&pw=' + ld.Password__c;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url2Redirect);
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        
        string loc = res.getHeader('Location');  
        
        Pagereference pageRedirect = new PageReference(loc);
        pageRedirect.setRedirect(true);
        return pageRedirect;*/
        }
        catch(Exception e){
            ApexPages.addMessages(new MyException(e.getMessage()));
            //return null;
        }
    }
    
    public AutoLoginPageController(ApexPages.StandardController stdController){
        //ld = (Login_Details__c)stdController.getRecord();
    }
    
    public static testMethod void AutoLoginPageController_Test(){
        Login_Details__c ld = new Login_Details__c(Username__c = 'test', Password__c = 'test', Environment__c = 'Sandbox');
        insert ld;
        //The Standard Controller - initialised with the record selected above 
        ApexPages.StandardController stc = new ApexPages.Standardcontroller(ld );
        
        //The ControllerExtension, initialised with the standard controller
        AutoLoginPageController s = new AutoLoginPageController(stc);
        Apexpages.currentPage().getParameters().put('Id',ld.Id);
        s.Login();      
    }
}