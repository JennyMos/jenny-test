public with sharing class AttendancesScreenController {
    
    public class MyException extends Exception {}
    
    public Work_hours__c MonthDate {get;set;}
    private Date oldMonthDate;
    private Id oldUserId;
    public List<List<Attendance__c>> allAttendances {get;set;}
    
    public Boolean SundayAttendance {get;set;}
    public Boolean MondayAttendance {get;set;}
    public Boolean TuedayAttendance {get;set;}
    public Boolean WeddayAttendance {get;set;}
    public Boolean ThudayAttendance {get;set;}
    public Boolean FridayAttendance {get;set;}
    public Boolean SatdayAttendance {get;set;}
    
    public AttendancesScreenController(){
        MonthDate = new Work_hours__c();
        MonthDate.Date__c = Date.today();
        MonthDate.User__c = UserInfo.getUserId();
        oldUserId = MonthDate.User__c;
        oldMonthDate = MonthDate.Date__c;
        setAttendances();       
        setAttendaceDays();
    }
    
    public PageReference setAttendaceDays(){
    	String days = [SELECT Working_Days__c FROM User WHERE Id = :MonthDate.User__c].Working_Days__c;
    	if(days == null)
    		days = '';
    	SundayAttendance = days.contains('Sun');
    	MondayAttendance = days.contains('Mon');
    	TuedayAttendance = days.contains('Tue');
    	WeddayAttendance = days.contains('Wed');
    	ThudayAttendance = days.contains('Thu');
    	FridayAttendance = days.contains('Fri');
    	SatdayAttendance = days.contains('Sat');
    	return null;
    }
    
    public PageReference setAttendances(){
    	allAttendances = new List<List<Attendance__c>>();
    	List<Attendance__c> temp = new List<Attendance__c>();
    	List<Attendance__c> att = [SELECT Id, Attendance__c,Date__c FROM Attendance__c WHERE User__c = :MonthDate.User__c AND CALENDAR_MONTH(Date__c) = :MonthDate.Date__c.month() AND CALENDAR_YEAR(Date__c) = :MonthDate.Date__c.year() ORDER BY Date__c ASC];
    	
    	Date d = MonthDate.Date__c.toStartOfMonth();
    	Integer i = 0;
    	//adding missing dates in month
    	while(d.addDays(i) < d.addMonths(1)){
    		if(att.size() <= i){
    			Attendance__c newa = new Attendance__c();
	    		newa.Date__c = d.addDays(i);
	    		att.add(newa);
    		}
    		else if(d.addDays(i) != att.get(i).Date__c){
	    		Attendance__c newa = new Attendance__c();
	    		newa.Date__c = d.addDays(i);
	    		att.add(i,newa);
    		}
    		i++;
    	}
    	//splitting into weeks
    	for(Attendance__c a : att){
    		if(getDatetimeFromDate(a.Date__c).format('EEEE') == 'Sunday' && (!temp.isEmpty())){
    			allAttendances.add(temp);
    			temp = new List<Attendance__c>();
    		}
    		temp.add(a);
    	}
    	allAttendances.add(temp);
    	//padding beginning of month until sunday
    	while(getDatetimeFromDate(allAttendances.get(0).get(0).Date__c).format('EEEE') != 'Sunday'){
    		Attendance__c newa = new Attendance__c();
    		newa.Date__c = allAttendances.get(0).get(0).Date__c.addDays(-1);
    		allAttendances.get(0).add(0,newa);
    	}
    	//padding end of month until saturday
    	while(getDatetimeFromDate(allAttendances.get(allAttendances.size()-1).get(allAttendances.get(allAttendances.size()-1).size()-1).Date__c).format('EEEE') != 'Saturday'){
    		Attendance__c newa = new Attendance__c();
    		newa.Date__c = allAttendances.get(allAttendances.size()-1).get(allAttendances.get(allAttendances.size()-1).size()-1).Date__c.addDays(1);
    		allAttendances.get(allAttendances.size()-1).add(newa);
    	}
    	return null;
    }
    
    private Datetime getDatetimeFromDate(Date d){
    	return datetime.newInstance(d.Year(),d.Month(), d.Day());
    }
        
    public PageReference Save(){
    	
        List<Attendance__c> att = new List<Attendance__c>();
        for(List<Attendance__c> la : allAttendances){
        	for(Attendance__c a : la){
        		if(a.Id != null){
        			att.add(a);		
        		}
        	}
        }
        try{
        	update att;
        	User u = [SELECT Working_Days__c FROM User WHERE Id = :MonthDate.User__c];
    		u.Working_Days__c = '';
    		if(SundayAttendance)
    			u.Working_Days__c += 'Sun;';
    		if(MondayAttendance)
    			u.Working_Days__c += 'Mon;';
    		if(TuedayAttendance)
    			u.Working_Days__c += 'Tue;';
    		if(WeddayAttendance)
    			u.Working_Days__c += 'Wed;';
    		if(ThudayAttendance)
    			u.Working_Days__c += 'Thu;';
    		if(FridayAttendance)
    			u.Working_Days__c += 'Fri;';
    		if(SatdayAttendance)
    			u.Working_Days__c += 'Sat;';
        	update u;
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your attendance was saved.'));
        }
        catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    
    
    public PageReference changedDate(){
        if(oldMonthDate.toStartOfMonth() != MonthDate.Date__c.toStartOfMonth()){
            oldMonthDate = MonthDate.Date__c;
            //Save();
            setAttendances();
        }
        return null;
    }
    public PageReference changedUser(){
        if(MonthDate.User__c != oldUserId){
            //Save();
            setAttendances();
            setAttendaceDays();
            oldUserId = MonthDate.User__c;
        }
        return null;
    }
    
    
    static testMethod void AttendancesScreenController_Test() {
        
        AttendancesScreenController con = new AttendancesScreenController();
        con.Save();
        con.changedDate();
        con.changedUser();
        
    }
}