/******************************************************************************* 
Name              : Edit Report WH Controller
Description       : Controller for the Edit Report WH Vf
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Hanit              10/05/2015                  Dana                  [SW-13316]
1. tom                12/07/2015                  Dana                  [SW-11522]
3. Jenny 			  31/01/2017				  Maor					[SW-22723]
*******************************************************************************/
public with sharing class EditReportWHController 
{
    public string recordId;
    public SFDC_Issue__c issue {get;set;}
    
    public EditReportWHController(ApexPages.StandardController stdController)
    {
        recordId = stdController.getId();
        issue = [Select Timer_Hours__c,Start_Time__c From SFDC_Issue__c Where Id=:recordId]; 
		
    }         
    public PageReference getFinishLocation() {
             if(isSF1())
                return  new PageReference('/' + recordId);
             return new PageReference('/' + recordId);
    }
    public static Boolean isSF1(){
        if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
        ){
            return true;
        }else{
            return false;
        }
	} 
}