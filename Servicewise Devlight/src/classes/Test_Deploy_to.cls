/************************************************************************************** 
Name              : Test_Deploy_to
Description       : test for Deploy_to_controllercls.cls
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. tom					11/06/2017				Eyal Morad				[SW-25308]
****************************************************************************************/
@isTest
private class Test_Deploy_to {

	@isTest
	private static void testDeployToFunc() {
       
		Account acc = new Account(Name = 'Test Account');		
		insert acc;
		Contact con = new Contact(LastName = 'Test Contact',Account = acc,ServiceWise_Personnel__c = true,Birthdate = Date.today().addYears(-25));
		insert con;
		SFDC_Project__c project = new SFDC_Project__c(QA_is_required_for_all_project_issues__c='No',Name = 'Test Project',SFDC_Project_Manager__c = '005D0000001GgJE',Project_Type__c = 'Internal',Account__c = acc.Id); //This is Einat's user Id.
    	insert project;		
		
		Releases__c release = new Releases__c(Name = 'test relise',release_date__c = System.today() );
		insert release;
		Login_Details__c login = new Login_Details__c(
														Username__c = 'test log in',
														Password__c = 'test 123',
														Environment__c = 'Salesforce Sandbox dev'
													 );
		insert login;
		SFDC_Issue__c issueToProd = new SFDC_Issue__c ( SFDC_Issue_Name__c = 'issue to production',
														SFDC_Project__c = project.Id,
														Issue_Type__c = 'Development',
														Issue_Sub_Type__c = 'New Feature',
														SFDC_Issue_Status__c = '2 - In Progress',
														Current_Environment__c = 'Production',
														SFDC_Issue_Priority__c = '1 - Low',
														Assigned_To__c = con.Id,
														Testing_Environment__c = 'dev',
														Due_Date__c = System.today(),
														release_name__c = release.Id,
														Test_Login_Details__c = login.Id
													  ); 
		//insert issueToProd;
		SFDC_Issue__c issueToUat = new SFDC_Issue__c (
														SFDC_Issue_Name__c = 'issue to production',
														SFDC_Project__c = project.Id,
														Issue_Type__c = 'Development',
														Issue_Sub_Type__c = 'New Feature',
														SFDC_Issue_Status__c = '2 - In Progress',
														Current_Environment__c = 'Production',
														SFDC_Issue_Priority__c = '1 - Low',
														Assigned_To__c = con.Id,
														Testing_Environment__c = 'dev',
														Due_Date__c = System.today(),
														Test_Login_Details__c = login.Id
										
													  ); //release_name__c = release.Id
		//insert issueToUat;
		List<SFDC_Issue__c> issueList = new List<SFDC_Issue__c>{issueToProd,issueToUat};
		insert issueList;
		for(SFDC_Issue__c issue : issueList){
			issue.Closing_notes_solution__c = 'Test';
			issue.SFDC_Issue_Status__c = '7 - Ready for Deploy';
		}
		update issueList;

		PageReference tpageRef = Page.Deployed_to;
		Test.setCurrentPage(tpageRef);
		ApexPages.currentPage().getParameters().put('rId',release.Id);
		ApexPages.currentPage().getParameters().put('to', 'p');

		Deploy_to_controllercls deployToProduction = new Deploy_to_controllercls();
		deployToProduction.startFlow();
		deployToProduction.cancel();

		issueToUat.release_name__c = release.Id;
		update issueToUat;
		PageReference uatPageRef = Page.Deployed_to;
		Test.setCurrentPage(uatPageRef);
		ApexPages.currentPage().getParameters().put('rId',release.Id);
		ApexPages.currentPage().getParameters().put('to', 'u');

		Deploy_to_controllercls deployToUat = new Deploy_to_controllercls();
		deployToUat.startFlow();
		deployToUat.cancel();

		PageReference changeStatusPageRef = Page.Deployed_to;
		Test.setCurrentPage(changeStatusPageRef);
		ApexPages.currentPage().getParameters().put('rId',release.Id);
		ApexPages.currentPage().getParameters().put('to', 'changeStatus');

		Deploy_to_controllercls changeReleaseStatus = new Deploy_to_controllercls();
		changeReleaseStatus.startFlow();
		changeReleaseStatus.cancel();
	}//testDeployToFunc
}