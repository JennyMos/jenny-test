global class ManageAttendances implements Schedulable {
    global void execute(SchedulableContext SC){
        List<Attendance__c> Att = [SELECT Id FROM Attendance__c WHERE Date__c < TODAY];
        delete Att;
        Att = [SELECT User__c,Id,Date__c FROM Attendance__c ORDER BY Date__c ASC];
        List<Attendance__c> NewAtt = new List<Attendance__c>();

        List<User> users = [SELECT Id,Working_Days__c FROM User WHERE IsActive = true];
        Date d = Date.Today();
        Set<Id> UserIds = new Set<Id>();
        for(User u : users)
            UserIds.add(u.Id);  
            
        Integer i = 0;
        Integer AttSize = Att.Size();
        while(Date.today().addDays(31) > d){
            Set<Id> TempUserIds = UserIds.clone();
            while(i < AttSize && Att[i].Date__c == d){
                TempUserIds.remove(Att[i].User__c);
                i++;
            }
            for(User u : users){
                if(TempUserIds.contains(u.Id)){
                    Attendance__c newa = new Attendance__c();
                    newa.User__c = u.Id;
                    newa.Date__c = d;
                    if(u.Working_Days__c == null)
                        newa.Attendance__c = false;
                    else
                        newa.Attendance__c = (u.Working_Days__c.contains(Datetime.newInstance(d,Datetime.now().time()).format('EEE')));
                    NewAtt.add(newa);
                }
            }   
            d = d.addDays(1);
        }
        insert NewAtt;      
        

    }
    
    static testmethod void test_ManageAttendances() {
	   Test.startTest();
	
	   // Schedule the test job 
	    
	
	      String jobId = System.schedule('testBasicScheduledApex',
	      '0 0 0 3 9 ? 2022', 
	         new ManageAttendances());
	   // Get the information from the CronTrigger API object 
	    
	
	      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
	         NextFireTime
	         FROM CronTrigger WHERE id = :jobId];
	
	   // Verify the expressions are the same 
	    
	      System.assertEquals('0 0 0 3 9 ? 2022', 
	         ct.CronExpression);
	
	   // Verify the job has not run 
	    
	      System.assertEquals(0, ct.TimesTriggered);
	
	   // Verify the next time the job will run 
	    
	      System.assertEquals('2022-09-03 00:00:00', 
	         String.valueOf(ct.NextFireTime));
	 
	
	   Test.stopTest();



   }
}