/****************************************************************************************
    Name              : LeadRegistrationDeleteBatchScheduler
    Description       : Scheduler to run batch for deleting lead registration recors
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue  
    ----------------------------------------------------------------------------------------
    1. Dana (created)            24/02/2016               Dana                 [SW-17823]
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
global class LeadRegistrationDeleteBatchScheduler implements Schedulable{
    global void execute(SchedulableContext sc){
        if([Select Id From AsyncApexJob a where JobType = 'BatchApex' AND ApexClass.Name = 'LeadRegistrationDeleteBatch' AND Status = 'Processing' limit 1].isEmpty()){
            Database.Batchable<sObject> leadregistrationssBatchable = new LeadRegistrationDeleteBatch();
            Database.executeBatch(leadregistrationssBatchable);
        }
    }

}