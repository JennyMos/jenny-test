/****************************************************************************************
    Name              : IssuesAssignScreenController
    Description       : 
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue      Description
    ----------------------------------------------------------------------------------------
    1. Nevo (UPDATED)              23/05/2016               Dana             [SW-17823]               
    
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
public with sharing class IssuesAssignScreenController {
    
    public class MyException extends Exception {}
    
    class Issue{
        /////////////////////////////
        public String TextName {get;set;}
        public String DueDateText {get; set;}
        public Date DueDate {get; set;}
        /////////////////////////////
        public String Name {get;set;}
        public String ProjectName {get;set;}
        public Decimal Priority {get;set;}
        public String Id {get;set;}
        public Issue(){}
    }
    
    class Resource{
        public String Name {get;set;}
        public String Color {get;set;}
        public String UserId {get;set;}
        public Resource(){}
    }
    
    public List<List<Issue>> allResourcesIssueLines {get;set;}
    public List<SFDC_Issue__c> allIncomingIssue {get;set;}
    public SFDC_Issue__c IssueDate {get;set;}
    public String ResourcesType {get;set;}
    public List<SelectOption> ResourcesTypeSO {get;set;}
    public List<Resource> allResources {get;set;}
    
    public IssuesAssignScreenController(){
        IssueDate = new SFDC_Issue__c();
        IssueDate.Due_Date__c = Date.today();
        ResourcesTypeSO = new List<SelectOption>();
        ResourcesTypeSO.add(new SelectOption('Consultant','Consultant'));
        ResourcesTypeSO.add(new SelectOption('Developer','Developer'));
        ResourcesType = 'Consultant';
        setupallResources();
        setupallIncomingIssue();
    }
    
    // Original
    public PageReference refresh(){
        setupallResources();
        setupallIncomingIssue();
        return null;
    }
    
    
    /* /// my refresh
    public PageReference refresh(){
        
        String tempResourcesType = ResourcesType;
        
        setupallResources();
        setupallIncomingIssue();
        
        ResourcesType = tempResourcesType;
        
        return null;
    }*/
    
    /////////////////////////////////
    /*
    public String subString(String str){
        String subStr;
        subStr = 'aaa';
        return subStr;
    }*/
    
    /////////////////////////////////
    
    
    public PageReference Save(){
        try{
            List<Id> IssuesIds = new List<Id>();
            for(List<Issue> il : allResourcesIssueLines){
                for(Issue i : il){
                    if(i.Id != '')
                        IssuesIds.add(i.Id);
                }
            }
            List<SFDC_Issue__c> updateIssues = [SELECT Internal_Priority__c FROM SFDC_Issue__c WHERE Id IN :IssuesIds];
            for(List<Issue> il : allResourcesIssueLines){
                for(Issue i : il){
                    if(i.Id != ''){
                        for(SFDC_Issue__c issue : updateIssues){
                            if(issue.Id == i.Id){
                                issue.Internal_Priority__c = i.Priority;
                            }
                        }
                    }
                }
            }
            
            update updateIssues;
            setupallResources();
            setupallIncomingIssue();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'All issues were saved.'));
            
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null; 
    }
    
    public PageReference setupallResources(){
        allResources = new List<Resource>();
        List<Id> allResourcesId = new List<Id>();
        List<String> allResourcesNames = new List<String>();
        
       for(User u : [SELECT Id,Name FROM User WHERE Resource_Type__c = :ResourcesType AND IsActive = true]){
     //for(User u : [SELECT Id,Name FROM User WHERE Resource_Type__c = :ResourcesType]){
            Resource r = new Resource();
            r.Name = u.Name;
            allResourcesNames.add(u.Name);
            r.UserId = u.Id;
            r.Color = '';
            allResources.add(r);
            allResourcesId.add(u.Id);           
        }
        
        for(Attendance__c a : [SELECT User__c,Attendance__c FROM Attendance__c WHERE User__c IN :allResourcesId AND Date__c = :IssueDate.Due_Date__c ]){
            for(Resource r : allResources){
                if(r.UserId == a.User__c){
                    if(a.Attendance__c)
                        r.Color = '#4fae21';  // old color -  r.Color = '#66FF00';
                    else
                        r.Color = '#da3232';  // old color - r.Color = '#FF9966';
                }
            }
        }
        
        List<Id> UsersContactId = new List<Id>();
        for(Contact c : [SELECT Id FROM Contact WHERE Name IN :allResourcesNames]){
            UsersContactId.add(c.Id);
        }
        List<String> Statuses = new List<String>();
        Statuses.add('0 - Open');
        Statuses.add('1 - Started');
        Statuses.add('2 - In Progress');
        Statuses.add('7 - Ready for Deploy');
        if(ResourcesType == 'Developer'){
            Statuses.add('8 - Ask for Estimation');
        }
        // old
        //List<SFDC_Issue__c> allIssues = [SELECT CreatedBy.Name,SFDC_Project__r.Name, Name, Internal_Priority__c,Assigned_To__r.Name FROM SFDC_Issue__c WHERE Assigned_To__c IN :UsersContactId AND SFDC_Issue_Status__c IN :Statuses ORDER BY Internal_Priority__c ASC];
        
        // new
        List<SFDC_Issue__c> allIssues = [SELECT CreatedBy.Name,SFDC_Project__r.Name, Name, Internal_Priority__c,Assigned_To__r.Name, SFDC_Issue_Name__c, Due_Date__c FROM SFDC_Issue__c WHERE Assigned_To__c IN :UsersContactId AND SFDC_Issue_Status__c IN :Statuses ORDER BY Internal_Priority__c ASC];
        ///
        allResourcesIssueLines = new List<List<Issue>>();
        
        while(allIssues.size() > 0)
        {
            List<Issue> tempIssuesLine = new List<Issue>();
            for(Resource r : allResources)  {
                Issue newissue = new Issue();
                newissue.Id = '';
                newissue.Name = '';
                Integer index = 0;
                for(SFDC_Issue__c i :allIssues){
                    if(i.Assigned_To__r.Name == r.Name){
                    
                        ////////////////////
                        newissue.TextName = i.SFDC_Issue_Name__c; 
                        if(newissue.TextName.length() > 10){
                            newissue.TextName = newissue.TextName.substring(0, 7);
                            newissue.TextName += '...';
                        
                        }
                        //newissue.DueDate = i.Due_Date__c;
                        // DueDateText
                        if(i.Due_Date__c != null ){ /// added this condition for issue sw-17823
                            Date temp = i.Due_Date__c;
                            String tempDate = String.valueOf(temp.day())+'/'+String.valueOf(temp.month())+'/'+String.valueOf(temp.year());
                            //temp.format('dd/MM/yyyy');
                            //newissue.DueDate = i.Due_Date__c;
                            //newissue.DueDate.format('dd/MM/yyyy');
                            newissue.DueDateText = tempDate;
                            
                            newissue.DueDate = i.Due_Date__c;
                        }
                        ////////////////////
                        
                        newissue.Id = i.Id;
                        newissue.Name = i.Name; 
                        newissue.Priority = i.Internal_Priority__c;
                        newissue.ProjectName = i.SFDC_Project__r.Name;
                        break;          
                    }
                    index++; 
                }
                if(index != allIssues.size())
                    allIssues.remove(index);
                tempIssuesLine.add(newissue);
            }
            allResourcesIssueLines.add(tempIssuesLine);
        }
        return null;
    }
        
    public PageReference setupallIncomingIssue(){
        List<String> Statuses = new List<String>();
        Statuses.add('0 - Open');
        Statuses.add('1 - Started');
        Statuses.add('2 - In Progress');
        allIncomingIssue = [SELECT CreatedBy.Name,Internal_Priority__c, Name, SFDC_Project__r.Name, SFDC_Issue_Name__c, Due_Date__c, CreatedDate, Assigned_To__r.Name FROM SFDC_Issue__c WHERE SFDC_Issue_Status__c IN :Statuses ORDER BY Internal_Priority__c ASC];
        return null;            
    }
    
    public PageReference changedType(){
    
        setupallResources();
        //setupallIncomingIssue();
        return null;
    }
    
    public PageReference changedDate(){
        setupallResources();
        return null;
    }   
    
    static testMethod void test_IssuesAssignScreenController() {
        IssuesAssignScreenController con = new IssuesAssignScreenController();
        con.changedDate();
        con.refresh();
        con.Save();
        con.setupallIncomingIssue();
    }
}