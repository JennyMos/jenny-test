/****************************************************************************************
    Name              : AccessServer
    Description       : 
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue      Description
    ----------------------------------------------------------------------------------------
    1. Gal (created)            11/12/2014               Dana             [SW-10678]                addIP2NetworkAccess
    2. Gal                      24/03/2015               Dana             [SW-10678]                removeIPfromNetworkAccess
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
public class AccessServer {

    private static Integer retries = 0;
    
    public AccessServer(){
        
    }
    
    public static String GetUserIPAddress() {
        string ReturnValue = '';
        ReturnValue = ApexPages.currentPage().getHeaders().get('True-Client-IP');       
        if (ReturnValue == null || ReturnValue == '') {
            ReturnValue = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } // get IP address when no caching (sandbox, dev, secure urls) 
        return ReturnValue;     
    }
    
    public void init(){
        String res = addIP2NetworkAccess(ApexPages.currentPage().getParameters().get('id'), Test.isRunningTest() ? '1.2.3.4' : GetUserIPAddress());
        if(res.startsWith('Success'))
            Apexpages.addMessage(New Apexpages.Message(Apexpages.Severity.CONFIRM, res));
        else
            Apexpages.addMessage(New Apexpages.Message(Apexpages.Severity.ERROR, res));
    }
    
    public String addIP2NetworkAccess(Id loginDetailsId, String ipAddress){
        try{
            Integer minutesToDeletion = Test.isRunningTest() ? 10 : Integer.valueOf(AccessServer__c.getInstance().Minutes_To_Deletion__c);
            Login_Details__c loginDetails = [select Name, Username__c, Password__c, Environment__c, Enable_Authorize_My_IP__c, Account__r.Name, Account__r.Enable_Authorize_My_IP__c from Login_Details__c where Id = :loginDetailsId];
            Boolean isProduction;
            //validations
            if(!loginDetails.Enable_Authorize_My_IP__c)
                return 'Authorize is disabled for environment "' + loginDetails.Name + '"';
            if(!loginDetails.Account__r.Enable_Authorize_My_IP__c)
                return 'Authorize is disabled for account "' + loginDetails.Account__r.Name + '"';
            if(ipAddress == null || ipAddress == '')
                return 'IP address is required';
            if(loginDetails.Username__c == null)
                return 'Username is required';
            if(loginDetails.Password__c == null)
                return 'Password is required';
            if( !loginDetails.Environment__c.contains('Salesforce Sandbox') && loginDetails.Environment__c != 'Salesforce Production')
                return 'Non Salesforce Environment';
            //encryption
            Long timestamp = System.now().getTime()/1000;
            String salt = String.valueOf(Crypto.getRandomInteger());
            String key = encryption(loginDetailsId, timestamp, salt);
            //callout
            Httprequest req = new Httprequest();
            req.setEndpoint(Test.isRunningTest() ? 'http://google.com' : AccessServer__c.getInstance().Endpoint__c);
            req.setMethod('POST');
            req.setBody('ld_id='+loginDetailsId+'&timestamp='+timestamp+'&key='+key+'&salt='+salt+'&ip='+ipAddress);
            req.setTimeout(120000);
            Http http = new Http();
            Httpresponse res;
            if(!Test.isRunningTest()) res = http.send(req);
            else{
                res = new Httpresponse();
                res.setBody('Success');
                res.setStatusCode(200);
            }
            //response
            if(res.getStatusCode() == 200){
                String body = res.getBody();
                if (body == 'Success'){
                    Datetime d = System.now().addMinutes(minutesToDeletion);
                    String cron = d.second() + ' ' + d.minute() + ' ' + d.hour() + ' ' + d.day() + ' ' + d.month() + ' ? ' + d.year();
                    String jobName = 'AccessServer_Remove_IP_' + loginDetailsId + '_' + ipAddress;
                    //remove previous jobs
                    Map<Id, CronJobDetail> existingJobs = new Map<Id, CronJobDetail>([select Id from CronJobDetail where Name = :jobName]);
                    if(existingJobs.size() > 0){
                        for(CronTrigger cronTrigger : [select Id from CronTrigger where CronJobDetailId IN :existingJobs.keySet()]){
                            System.abortJob(cronTrigger.Id);
                        }
                    }
                    //schedule the deletion
                    String jobId = System.schedule(jobName, cron, new AccessServer_Schedulable(loginDetailsId, ipAddress));
                    return 'Success. you have '+minutesToDeletion+' minutes window to login.';
                } 
                return body;
            }
            else if (res.getStatusCode() == 503 && retries < 1){ //Service Unavailable
                retries++;
                return addIP2NetworkAccess(loginDetailsId, ipAddress);
            }
            else
                return res.getStatus();
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    
    private static String encryption(Id loginDetailsId, Long timestamp, String salt){
        String secret = 'hXg0yb43c06yzu21N8810rY4swn6WNgM';
        String input = loginDetailsId+'#$%'+salt+'#$%'+timestamp+'Bond, James Bond.';
        System.debug(input);
        Blob encryptData = crypto.generateMac('HmacSHA256', Blob.valueOf(input), Blob.valueOf(secret));
        return EncodingUtil.convertToHex(encryptData);
    }
    
    @future (callout=true)
    public static void removeIPfromNetworkAccessFuture(Id loginDetailsId, String ipAddress){
        removeIPfromNetworkAccess(loginDetailsId, ipAddress);
    }
    
    @TestVisible
    private static void sendAlertByEmail(Id loginDetailsId, String ipAddress, String errorMessage){
        Login_Details__c loginDetails = [select Id, Account__c from Login_Details__c where Id = :loginDetailsId];
        List<SFDC_Project__c> projects = [select Id, Owner.Email from SFDC_Project__c where SFDC_Project_Status__c = 'In progress' and Account__c = :loginDetails.Account__c];
        set<String> ccEmails = new Set<String>();
        for(SFDC_Project__c proj : projects)
            ccEmails.add(proj.Owner.Email);
        Messaging.reserveSingleEmailCapacity(1+ccEmails.size());
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(Test.isRunningTest())
            mail.setToAddresses(new List<String>{'sample@email.com'});
        else
            mail.setToAddresses(new List<String>{AccessServer__c.getInstance().Alert_Email_Address__c});
        if(ccEmails.size() > 0)
            mail.setCcAddresses(new List<String>(ccEmails));
        mail.setReplyTo('no-reply@salesforce.com');
        mail.setSenderDisplayName('Authorize My IP System');
        mail.setSubject('IP removal failed');
        mail.setBccSender(false);
        mail.setUseSignature(false);        
        mail.setPlainTextBody('IP removal failed for login details: ' + loginDetailsId +' and IP address: ' + ipAddress + '. Error message: ' + errorMessage);
        mail.setHtmlBody('IP removal failed for login details: <b>' + loginDetailsId + '</b> and IP address: ' + ipAddress + '. Error message: ' + errorMessage + '<p>' +
             'To view the login details <a href=https://eu1.salesforce.com/'+loginDetailsId+'>click here.</a>');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private static void removeIPfromNetworkAccess(Id loginDetailsId, String ipAddress){
        try{
            //encryption
            Long timestamp = System.now().getTime()/1000;
            String salt = String.valueOf(Crypto.getRandomInteger());
            String key = encryption(loginDetailsId, timestamp, salt);
            //callout
            Httprequest req = new Httprequest();
            req.setEndpoint(Test.isRunningTest() ? 'http://google.com' : AccessServer__c.getInstance().Endpoint__c);
            req.setMethod('POST');
            req.setBody('ld_id='+loginDetailsId+'&timestamp='+timestamp+'&key='+key+'&salt='+salt+'&ip='+ipAddress+'&mode=remove');
            req.setTimeout(120000);
            Http http = new Http();
            Httpresponse res;
            if(!Test.isRunningTest()) res = http.send(req);
            else{
                res = new Httpresponse();
                res.setBody('Success');
                res.setStatusCode(200);
            }
            //response
            if(res.getStatusCode() == 200){
                String body = res.getBody();
                if (body != 'Success'){
                    if (retries < 3){
                        retries++;
                        removeIPfromNetworkAccess(loginDetailsId, ipAddress);
                    }
                    else{
                        //sendAlertByEmail(loginDetailsId, ipAddress, res.getBody());
                    }
                }
            }
            else if(retries < 3){
                retries++;
                removeIPfromNetworkAccess(loginDetailsId, ipAddress);
            }
            else{
               // sendAlertByEmail(loginDetailsId, ipAddress, res.getStatus());
            }
            system.debug('res='+res+';body='+res.getBody());
        }
        catch(Exception e){
            system.debug('Exception='+e);
            //sendAlertByEmail(loginDetailsId, ipAddress, e.getMessage());
        }
    }
}