/**************************************************************************************
Name              : Lead_CreateCampaignMemberFromUnbouncedCampaign
Description       : Create campaign member with: LeadId, Status='Registered', and Unbounced CampaignId.
Revision History  :	-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Irit                 22/07/2013              Mari Schuller          [SW-5663]
****************************************************************************************/
@isTest
private class Lead_CreateCampaignMember_Test 
{
    static testMethod void myUnitTest() 
    {
       Campaign camp = new Campaign(Name = 'TestCampaign');
       insert camp;
       Lead lead = new Lead(LastName = 'TestLead', Company = 'TestCompany', LeadSource = 'Web',Unbounced_Campaign_ID__c = camp.Id);
       Test.startTest();
       insert lead;
       Test.stopTest();
       List<CampaignMember> cmList = new List<CampaignMember>([Select LeadId,CampaignId,Status From CampaignMember Where CampaignId =: camp.Id And LeadId =:lead.Id ]);
       if(cmList.isEmpty())
       {
       		system.assert(true);	
       }
    }
}