/************************************************************************************** 
Name              : ReportRecurrentJobWHCtrlLightning_Test 
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny	              03/08/2017               Dana                  [SW-25619]
****************************************************************************************/
@isTest
public class ReportRecurrentJobWHCtrlLightning_Test {
	static testMethod void Controller_Test(){ 
		Account acc = new Account(Name = 'acc name'/*, MigrationTest__c = '345345'*/);
		insert acc;
		SFDC_Project__c proj =  new SFDC_Project__c(Name='Test Project',Account__c = acc.Id,QA_is_required_for_all_project_issues__c = 'No');
		insert proj;
		SFDC_Issue__c issue = new SFDC_Issue__c(SFDC_Issue_Name__c='Tests',SFDC_Project__c = proj.Id);
		insert issue;
		Work_hours__c wh = new Work_hours__c(Project_name__c = proj.Id,Date__c = system.today(),Description__c ='',Account__c = acc.Id,Issue_Number__c = issue.Id,Hours__c=5,User__c= system.UserInfo.getUserId());

		proj = ReportRecurrentJobWHCtrlLightning.getProjectName(proj.Id);
		Boolean isdAysoff = ReportRecurrentJobWHCtrlLightning.CheckIfIsDayOffProj(proj.Id);
		String whstring = JSON.serialize(wh);
		List<Date> dates = ReportRecurrentJobWHCtrlLightning.SetDays(whstring,'2017-07-25');
		wh.From_Date__c = dates[0];
		wh.To_Date__c = dates[1];
		whstring = JSON.serialize(wh);
		String res = ReportRecurrentJobWHCtrlLightning.SaveCtr(whstring);
		List<String> pickvalues = ReportRecurrentJobWHCtrlLightning.GetPickListValue('Work_hours__c','AbsenceReason__c',true);

	}
}