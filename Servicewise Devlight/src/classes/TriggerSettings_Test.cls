@isTest
public with sharing class TriggerSettings_Test
{  
static testMethod void TriggersSettings_IsTriggerActive_Test()
    {
        Profile p = [Select id from profile where Name='System Administrator'];
        triggers_to_turn_off__c triggersToTurnOff = new triggers_to_turn_off__c();
        triggersToTurnOff.Name = 'Test';
        triggersToTurnOff.Trigger_API_Name__c = 'Test';
        triggersToTurnOff.Function_name__c = 'test';
        triggersToTurnOff.Profile_ByPass_Triggers__c = String.valueOf(p.id);
        insert triggersToTurnOff;
        
        Boolean condition = TriggerSettings.IsTriggerActive('Test' , 'test');
        system.assertEquals(condition , true);

         Profile p2 = [Select id from profile where Name='Standard User'];
        triggers_to_turn_off__c triggersToTurnOff2 = new triggers_to_turn_off__c();
        triggersToTurnOff2.Name = 'Test2';
        triggersToTurnOff2.Trigger_API_Name__c = 'Test2';
        triggersToTurnOff2.Function_name__c = 'test2';
        triggersToTurnOff2.Profile_ByPass_Triggers__c = String.valueOf(p2.id);
        insert triggersToTurnOff2;

        Boolean condition3 = TriggerSettings.IsTriggerActive('Test2' , 'test2');
        system.assertEquals(condition3 , true);

    }
}