@isTest
private class LeadRegistrationDeleteBatchTests {

/*
    static testmethod void LeadRegistrationDeleteBatch_Test(){   
        Lead_registration__c lr = new Lead_registration__c(name = 'test', Lead_ID__c = '111');
        insert lr;      
    
        Test.startTest();       
        LeadRegistrationDeleteBatch b = new LeadRegistrationDeleteBatch();
        Id batchprocessid = Database.executeBatch(b);            
        Test.stopTest();   
        }
       
          */  
     static testMethod void LeadRegistrationDeleteBatchScheduler_Test() {     
        Test.startTest();        
        String jobId = System.schedule('testBasicScheduledApex', '0 0 0 3 9 ? 2042', new LeadRegistrationDeleteBatchScheduler());
        Test.stopTest();        
    }  
        
}