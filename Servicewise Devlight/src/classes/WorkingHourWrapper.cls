public class WorkingHourWrapper {
		@AuraEnabled public Boolean isCompleted {get;set;}
        @AuraEnabled public List<String> ErrorMessages {get;set;}
        @AuraEnabled public Boolean noError {get;set;}
        @AuraEnabled public Integer WHID {get;set;}
        @AuraEnabled public Boolean isWHIDnull {get;set;}
        @AuraEnabled public Id WorkingHourId {get;set;}
        @AuraEnabled public Id Project {get;set;}
        @AuraEnabled public String ProjectName {get;set;}
        @AuraEnabled public Id AccountId {get;set;}
        @AuraEnabled public Double Hours {get;set;}
        @AuraEnabled public Double OldHours {get;set;}
        @AuraEnabled public Date WHDate {get;set;}
        @AuraEnabled public String Description {get;set;}
        @AuraEnabled public Boolean Billable {get;set;}
        @AuraEnabled public String TravelWay {get;set;}
        @AuraEnabled public string AbsenceReason { get; set; }
        @AuraEnabled public Id IssueNumber {get;set;}
    	@AuraEnabled public String InternalComments {get;set;}
        @AuraEnabled public String CustomerDescription {get;set;}
        public WorkingHourWrapper(Boolean isCompleted ,Id IssueNumber, String ProjectName,Id AccountId,Id WorkingHourId,Integer WHID,Id Project, Decimal Hours, String Description, Boolean Billable, Boolean Vacation, Boolean Sickness, String TravelWay, string AbsenceReason, Date WHDate,String InternalComments, String CustomerDescription )
        {
            noError = true;
            ErrorMessages = new List<String>();
			this.CustomerDescription = CustomerDescription;
            this.isCompleted = isCompleted;
            this.IssueNumber = IssueNumber;
            this.OldHours = Hours;
            this.ProjectName = ProjectName;
            this.AccountId = AccountId;
            this.WorkingHourId = WorkingHourId;
            this.WHID = WHID;
            this.isWHIDnull = (WHID == null);
            this.WHDate = WHDate;
            this.Project = Project;
            this.Hours = Hours;
            this.Description = Description;
            this.Billable = Billable;
            this.TravelWay = TravelWay;
            this.AbsenceReason = AbsenceReason;
            this.InternalComments = InternalComments; 
        }
    	public Boolean Validate (Boolean isDaysOff){

			System.debug('isDaysOff'+isDaysOff) ;
            noError = true;
        	ErrorMessages = new List<String>();
            if(Hours != 0){
               if ((AbsenceReason == null || AbsenceReason == '--Select--') && isDaysOff)
                {
                    ErrorMessages.add('You Need to add Absence Reson.');
                    noError = false;
                }
				if ((CustomerDescription == null || CustomerDescription == 'None' || CustomerDescription == '--Select--' || CustomerDescription=='')  && !isDaysOff)
                {
                    ErrorMessages.add('You must select description for the customer.');
                    noError = false;
                }
                if((Description == null || Description.trim() == '') && (String)IssueNumber =='' && !isDaysOff){
                    ErrorMessages.add('You must enter Description when there is no issue');
                    noError = false;
                    system.debug('desc null error');
                }
                if(Description != null && Description.length() > 255){
                    ErrorMessages.add('Description is too long, maximum of 255 characters.');
                    noError = false;
                }
                Integer devidey = Integer.valueOf(Hours*100);
                if(math.mod(devidey,25) != 0){
                    ErrorMessages.add('Working Hour is invalid, please round to the closest number that divides by 0.25');
                    noError = false;
                }
            }
            system.debug('errors:'+ ErrorMessages); 
         return noError;  
   		}
        
}