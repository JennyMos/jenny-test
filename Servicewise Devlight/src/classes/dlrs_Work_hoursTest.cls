/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Work_hoursTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Work_hoursTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Work_hours__c());
    }
}