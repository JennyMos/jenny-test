public class ReportRecurrentJobWHCtrlLightning {

	 @AuraEnabled
	 public static SFDC_Project__c getProjectName(String projectId)
		{
			return [select id, Name, Account__c from SFDC_Project__c where id = :projectId];
		}

	 @AuraEnabled
	 public static Boolean CheckIfIsDayOffProj(String projectId)
	{
        for(ServiceWiseDayOffProject__c dop : ServiceWiseDayOffProject__c.getAll().values())
        {
			system.debug('dop.ProjectId__c = ' + dop.ProjectId__c);
        	if (projectId.contains(dop.ProjectId__c))
				return true;
        }
		return false;
	}

	 @AuraEnabled
	public static List<Date> SetDays(String whjson, String dateStr)
	{
		Work_hours__c wh = (Work_hours__c)JSON.deserialize(whjson, Work_hours__c.class );
        //'2017-07-25'
        system.debug('dateStr = ' + dateStr);
		system.debug('dateStr = ' + dateStr.substring(0,4));
		system.debug('dateStr = ' + dateStr.substring(6,7));
		system.debug('dateStr = ' + dateStr.substring(9,10));
		Datetime dt = Datetime.newInstance(integer.valueof(dateStr.substring(0,4)),integer.valueof(dateStr.substring(5,7)),integer.valueof(dateStr.substring(8,10)),  3,3,3);
		system.debug('dt = ' + dt);
		wh.From_Date__c = dt.dateGmt().toStartOfWeek();
		wh.To_Date__c = wh.From_Date__c.AddDays(4);
        return new List<Date>{wh.From_Date__c ,wh.To_Date__c};
	}
	 @AuraEnabled
	public static String SaveCtr(String whjson)
	{
        system.debug('wh:'+ whjson);
        Work_hours__c wh = (Work_hours__c)JSON.deserialize(whjson, Work_hours__c.class );
		list<Work_hours__c> whToInsert = new list<Work_hours__c>();
		for (Date a = wh.From_Date__c; a<= wh.To_Date__c; a = a.addDays(1))
		{
			Work_hours__c newWH = wh.clone();
			newWH.Date__c = a;
			newWH.User__c = UserInfo.getUserId();
			whToInsert.add(newWH);
		}
		system.debug('whToInsert == ' + whToInsert);
		if (whToInsert.size() > 0)
		{
			try
			{
				insert whToInsert;
				return 'success';
			}
			catch (exception e)
			{
		        return e.getmessage();
			}
		}
		return '';
	}

	 @AuraEnabled
	public static List<String> GetPickListValue(string objectType,string picklistName,boolean hasNoneValue){
		 return MassWorkingHoursControllerLightning.GetPickListValue(objectType, picklistName,hasNoneValue);
	}
}