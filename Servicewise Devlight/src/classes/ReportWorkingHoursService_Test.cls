/************************************************************************************** 
Name              : TestMethods1 
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny	              30/03/2017               Dana                  [SW-23982]
****************************************************************************************/
@isTest(SeeAllData=true)
private class ReportWorkingHoursService_Test  {

    static testMethod void EmailService_Test(){

		Account acc = new Account(Name = 'acc name');
		insert acc;
		SFDC_Project__c proj =  new SFDC_Project__c(Name='Test Project',Account__c = acc.Id,QA_is_required_for_all_project_issues__c = 'No');
		insert proj;
		String delimiter = ';';
        // create a new email and envelope object
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       
       // setup the data for the email
     
      email.fromAddress = 'someaddress@email.com';
      email.plainTextBody = 'email body\n2225256325\nTitle'; 
      // add a Text atatchment
  
      List<WHEmailServicMapping__mdt> fieldsMapping = [select attachmentColumnHeader__c, fieldApiName__c from WHEmailServicMapping__mdt ];
	  Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
	  attachmenttext.body ='';
	  attachmenttext.fileName = 'textfiletwo3.csv';
	  ReportWorkingHoursService  testInbound = new ReportWorkingHoursService ();

	   //0. no subject
	  for (WHEmailServicMapping__mdt fildname :fieldsMapping){
			attachmenttext.body += fildname.attachmentColumnHeader__c + delimiter; 
	  }
      email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
      testInbound=new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*--------------------------------------------------------------------------------------------------------------------------------------------*/
	  //1. no attachment, but has subject
      email.textAttachments = null;
	  email.subject = 'Tal Uri';
	  testInbound=new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*--------------------------------------------------------------------------------------------------------------------------------------------*/
	  //2.There was a problem with the attachment format
	  attachmenttext.mimeTypeSubType = 'text/txt';
	  email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
	  testInbound=new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*--------------------------------------------------------------------------------------------------------------------------------------------*/
	  // 3. proper attachment with two lines:one 
	  attachmenttext.body ='';
	  for (WHEmailServicMapping__mdt fildname :fieldsMapping)
			attachmenttext.body += fildname.attachmentColumnHeader__c + ','; 
	  attachmenttext.body = attachmenttext.body.substringBeforeLast(',');
      attachmenttext.body += ' \n 01/05/2017'+ delimiter + 'Description'+ delimiter+'5'+ delimiter + 'Internal'+ delimiter+proj.Id+' \n 01/05/2017'+ delimiter + 'Description'+ delimiter + '3'+ delimiter + 'Internal'+ delimiter + proj.Id; 
      attachmenttext.fileName = 'textfiletwo3.csv';
      attachmenttext.mimeTypeSubType = 'text/csv';
      email.textAttachments =  new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
	  testInbound.handleInboundEmail(email, env);
      /*--------------------------------------------------------------------------------------------------------------------------------------------*/
	  //4. attachmnet with only one column
	  attachmenttext.body = fieldsMapping[0].attachmentColumnHeader__c + '\n'+ '1345 \n' ; 
	  email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
      testInbound=new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*---------------------------------------------------------------------------------------------------------------------------------------------*/
	  //5. Attached Table Has Illegal Values
	  attachmenttext.body ='';
	  for (WHEmailServicMapping__mdt fildname :fieldsMapping){
			attachmenttext.body += fildname.attachmentColumnHeader__c + ','; 
	  }
      attachmenttext.body +=  delimiter + ' \n'+ delimiter + '5'+ delimiter + '31/08/2016'+ delimiter + '1'; 
      email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
      testInbound = new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*---------------------------------------------------------------------------------------------------------------------------------------------*/
	   //6. VR error
	  attachmenttext.body ='';
	  for (WHEmailServicMapping__mdt fildname :fieldsMapping){
			attachmenttext.body += fildname.attachmentColumnHeader__c + delimiter;
	  }
      attachmenttext.body += ' \n 01/05/2017'+ delimiter + 'Description'+ delimiter + '5.3'+ delimiter + 'Internal' + delimiter + proj.Id;
	  email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
      testInbound=new ReportWorkingHoursService ();
      testInbound.handleInboundEmail(email, env);
	  /*---------------------------------------------------------------------------------------------------------------------------------------------*/
	 
    }
}