public class UpdateInvoicesController {
  

  public Invoice_c__c invoice {get; set;}
  public List<Invoice_c__c> ToBeInvoicedList {get; set;}
  public Boolean ToShowToBeInvoicedList {get; set;}
  public Integer stage {get; set;}
  public String InvoiceId {get; set;}
  
  public class MyException extends Exception {}

  public UpdateInvoicesController(){
    invoice = new Invoice_c__c();
    stage = 1;
    ToShowToBeInvoicedList = false;
  }
  
  public PageReference ShowToBeInvoicedList(){
  	ToBeInvoicedList = [SELECT Actual_Payment_Date__c,Billing_Contact__r.Email,Billing_Contact__r.Name,Unit_Price__c,Quantity__c,Amount_with_VAT__c,Invoice_Status__c,Account_Name__r.Name,Name,Id,Invoice_Date__c,Invoice_Amount__c,Description__c FROM Invoice_c__c WHERE Invoice_Status__c = 'To Be Invoiced' AND To_invoice_externally__c = 'Yes'];
  	if(ToBeInvoicedList.isEmpty()){
  		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'There are no invoices with status "To Be Invoiced".'));
  	}
  	else{
  		ToShowToBeInvoicedList = true;
  	}
  	return Apexpages.currentPage();
  }
  
  public PageReference ClearToBeInvoicedList(){
  	ToShowToBeInvoicedList = false;
  	return Apexpages.currentPage();
  }
  
  public void SelectInvoice(){
  	for(Invoice_c__c inv : ToBeInvoicedList){
  		if(inv.Id == InvoiceId){
  			Invoice = inv;
  			stage = 2;

  		}
  	}
  }
  
  public PageReference getInvoice(){
    List<Invoice_c__c> invoices = [SELECT Actual_Payment_Date__c,Billing_Contact__r.Email,Billing_Contact__r.Name,Unit_Price__c,Quantity__c,Amount_with_VAT__c,Account_Name__r.Name,Name,Id,Invoice_Date__c,Invoice_Amount__c,Invoice_Status__c,Description__c FROM Invoice_c__c WHERE Name = :invoice.Name];
    if(invoices.size() == 0){
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'There is no invoice with this invoice number. Please try again.'));
    }
    else{
    	invoice = invoices[0];
    	stage = 2;
    }
    if(invoices.size() > 1){
    	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,'There is more then one invoice with this invoice number.'));
    }
  	return Apexpages.currentPage();
  }
  
  public PageReference invoiced(){
  	stage = 3;
  	return Apexpages.currentPage();
  }
  
  public PageReference back(){
  	stage = 2;
  	return Apexpages.currentPage();
  }
  
  public PageReference save_invoice(){
  	invoice.Invoice_Status__c = 'Invoiced';
  	update invoice;
  	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'Invoice updated successfuly.'));
  	return clear();
  }
  
  public PageReference save_paid(){
  	invoice.Invoice_Status__c = 'Paid';
  	update invoice;
  	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'Invoice updated successfuly.'));
  	return clear();
  }
  
  public PageReference paid(){
  	stage = 4;
  	return Apexpages.currentPage();
  }
  
  public PageReference clear(){
  	invoice = new Invoice_c__c();
  	stage = 1;
  	ToShowToBeInvoicedList = false;
  	return Apexpages.currentPage();
  }
  
  static testMethod void test_UpdateInvoicesController() {
  	Account a = new Account(Name='Test');
    insert a;
  	Invoice_c__c inv = new Invoice_c__c(Account_Name__c = a.Id);
  	insert inv;
  	UpdateInvoicesController con = new UpdateInvoicesController();	
  	con.ShowToBeInvoicedList();
  	con.ClearToBeInvoicedList();
  	con.SelectInvoice();
  	con.getInvoice();
  	con.invoiced();
  	con.back();
  	con.invoice = inv;
  	con.save_invoice();
  	con.invoice = inv;
  	con.save_paid();
  	
  }
}