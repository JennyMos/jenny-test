@isTest
public class UpdateAccountSiteManagerBatch_test {
	@isTest
	public static void testbatch (){

		Account acc = new Account(Name = 'Test Account', Site_Manager_Changed__c = true);		
		insert acc;
		Contact con = new Contact(LastName = 'Test Contact',First_Name_Hebrew__c = 'יידיג',Account = acc,ServiceWise_Personnel__c = true, Birthdate= system.today() );
		insert con;
		Invoice_c__c inv = new Invoice_c__c(Account_Name__c = acc.Id);
		insert  inv;
		Purchase_Order__c po = new Purchase_Order__c(Account__c = acc.Id,Amount__c = 100, Contact__c = con.Id);
		insert po;

        String jobId = System.schedule('testBasicScheduledApex','0 0 0 3 9 ? 2022', new UpdateAccountSiteManagerScheduler());
		UpdateAccountSiteManagerBatch b = new UpdateAccountSiteManagerBatch(); 
      	database.executebatch(b,50);
	}
}