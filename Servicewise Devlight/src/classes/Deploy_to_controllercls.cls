/************************************************************************************** 
Name              : Deploy_to_controllercls
Description       : controller for Deployed_to.page
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. tom					11/06/2017				Eyal Morad				[SW-25308]
2. Yuval				13/06/2017				Eyal Morad				[SW-25308]
****************************************************************************************/
public class Deploy_to_controllercls {
	public Boolean isprode {get;set;}
	public String env {get;set;}
	public Boolean changeStatus {get;set;}
	public String rId {get;set;}
	//public Flow.Interview.Deployed_to_Production myFlow {get; set;}
 
	public Deploy_to_controllercls(){

		rId = ApexPages.currentPage().getParameters().get('rId'); 
		env = ApexPages.currentPage().getParameters().get('to');
		System.debug('env =>> '+env);
		if(env == 'p'){
			isprode = true;
			changeStatus = false;
			env = 'Are you sure you want to change all related issues RFQA after deployment in the production?';
		}
		if(env == 'u'){
			isprode = false;
			changeStatus = false;
			env = 'Are you sure you want to change all related issues RFQA in the new environment?';
		}
		if(env == 'changeStatus'){
			changeStatus = true;
			env = 'Are you sure you want to close the release and change all related issues to close?';
		}
	}//Deploy_to_controllercls

	public PageReference startFlow() {
	System.debug('isprode1 ==> '+isprode);
		if(!changeStatus){
			Map<String,Object> parmes = new Map<String,Object>();
		
			parmes.put('ReleaseID',ApexPages.currentPage().getParameters().get('rId'));
			if(isprode){//if deploy to production
				Flow.Interview.Deployed_to_Production myFlow = new Flow.Interview.Deployed_to_Production(parmes);
				try{
					myFlow.start();
		
				}catch(Exception e){
					System.debug(' Exception ==> '+e.getMessage());
				}
			}else{
				System.debug('isprode ==> '+isprode);
				Flow.Interview.Deployed_to_Full_UAT_envi myFlow = new Flow.Interview.Deployed_to_Full_UAT_envi(parmes);
				try{
					myFlow.start();
		
				}catch(Exception e){
					System.debug(' Exception ==> '+e.getMessage());
				}
			}
		}else{
			List<Releases__c> getRelease = [Select Id, Release_Status__c From Releases__c Where Id =: rId];
			System.debug('getRelease===>'+getRelease);
			if(getRelease != null && getRelease.size() > 0){
				getRelease[0].Release_Status__c = 'Complete';
				update getRelease[0];
			}
		}

		return new PageReference('/'+rId);
	}

		public PageReference cancel() {
			return new PageReference('/'+rId);
		}
}