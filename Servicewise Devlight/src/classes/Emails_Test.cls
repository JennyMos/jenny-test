@isTest
public with sharing class Emails_Test{

static testMethod void CreateIsuueFromEmail_Test() {
  
        Messaging.inboundEmail email = new Messaging.inboundEmail();
        email.subject = 'New Issue to help desk from test';
        email.plainTextBody = 'test';
        email.fromAddress = 'dana@service-wise.com';
      
        CreateIssueFromEmail issue1 = new CreateIssueFromEmail();
      
        test.startTest();
        
        Messaging.InboundEmailResult result = issue1 .handleInboundEmail(email, null);
        
        test.stopTest();
      
        System.assert(result.success);

    }
    
}