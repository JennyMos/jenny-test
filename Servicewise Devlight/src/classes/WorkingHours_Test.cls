@isTest
private class WorkingHours_Test {
    
    @testSetup static void setup() {
    
    }

    @isTest
    private static void testExpectedAmountOfOurs() {
        
        //create controller instance.
        MessWorkingHoursController ctrl = new MessWorkingHoursController();
        ctrl.WorkHours.Date__c = Date.today();
        /**
        * TEST 0
        * no working hour, check that we have 0.0 expected amount of hours.
        */
        Double expectedAmountOfHours = ctrl.getExpectedAmountOfHours();
        System.assertEquals(expectedAmountOfHours,0.0);

        //create working hour standard object.
        String currentMonth = System.now().format('MMMM'); // get current month.
        Working_Hour_Standard__c expectedHours = new Working_Hour_Standard__c(Name=currentMonth,Amount_of_Hours__c=10);
        insert expectedHours;

        /**
        * TEST 1
        * check that we got the expected working as the current month.
        */
        expectedAmountOfHours = ctrl.getExpectedAmountOfHours();
        System.assertEquals(expectedAmountOfHours,10.0);

        /**
        * TEST 2
        * change the working hour screen to the next month as check that we didn't got result.
        */
        String nextMonth = System.now().addMonths(1).format('MMMM');
        expectedHours.Name = nextMonth;
        update expectedHours;
        expectedAmountOfHours = ctrl.getExpectedAmountOfHours();
        System.assertNotEquals(expectedAmountOfHours,10);

    }

    @isTest
    private static void testReportedAmountOfOurs() {
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

        User usr = new User(LastName = 'TEST',
            FirstName='USER',
            Alias = 'tester',
            Email = 'User@sw.com',
            Username = 'User@sw.com',
            ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LocaleSidKey = 'en_US'
        );

        insert usr;

        //create controller instance.
        MessWorkingHoursController ctrl = new MessWorkingHoursController();
        ctrl.WorkHours.Date__c = Date.today();
        Id user_id = UserInfo.getUserId();

        //create working hour releated objects.
        Account acc = new Account(Name = 'Test Account');       
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact',Account = acc,ServiceWise_Personnel__c = true,Birthdate = Date.today().addYears(-25));
        insert con;
        SFDC_Project__c project = new SFDC_Project__c(QA_is_required_for_all_project_issues__c='No',Name = 'Test Project',SFDC_Project_Manager__c = '005D0000001GgJE',Project_Type__c = 'Internal',Account__c = acc.Id); //This is Einat's user Id.
        insert project;

        Work_hours__c workhour = new Work_hours__c();
        workhour.Project_name__c = project.Id;
        workhour.Hours__c = 9;
        workhour.User__c = user_id;
        workhour.Date__c = Date.today();
        insert workhour;

        Work_hours__c workhour2 = new Work_hours__c();
        workhour2.Project_name__c = project.Id;
        workhour2.Hours__c = 1.25;
        workhour2.User__c = user_id;
        workhour2.Date__c = Date.today();
        insert workhour2;

        //different month, should not be calculated.
        Work_hours__c workhour3 = new Work_hours__c();
        workhour3.Project_name__c = project.Id;
        workhour3.Hours__c = 5;
        workhour3.User__c = user_id;
        workhour3.Date__c = Date.today().addMonths(2);
        insert workhour3;

        //different user same mont
        Work_hours__c workhour4 = new Work_hours__c();
        workhour4.Project_name__c = project.Id;
        workhour4.Hours__c = 5;
        workhour4.User__c = usr.id;
        workhour4.Date__c = Date.today();
        insert workhour4;

        /**
        * check that we got the reported working as the current month.
        */
        Double reportedAmountOfHours = ctrl.getReportedAmountOfHours();
        System.assertEquals(reportedAmountOfHours,10.25);
    }

    @isTest
    private static void testCalcReportedAmountOfOurs() {
        
        //create controller instance.
        MessWorkingHoursController ctrl = new MessWorkingHoursController();
        ctrl.WorkHours.Date__c = Date.today();
        Id user_id = UserInfo.getUserId();
        //create working hour standard object.
        List<Work_hours__c> workDays = new List<Work_hours__c>();

        Double calcHours = ctrl.calcReportedAmountOfOurs(workDays);
        System.assertEquals(calcHours,0.0);

        //create working hour releated objects.
        Account acc = new Account(Name = 'Test Account');       
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact',Account = acc,ServiceWise_Personnel__c = true,Birthdate = Date.today().addYears(-25));
        insert con;
        SFDC_Project__c project = new SFDC_Project__c(QA_is_required_for_all_project_issues__c='No',Name = 'Test Project',SFDC_Project_Manager__c = '005D0000001GgJE',Project_Type__c = 'Internal',Account__c = acc.Id); //This is Einat's user Id.
        insert project;

        //create working hour.
        Work_hours__c workhour = new Work_hours__c();
        workhour.Project_name__c = project.Id;
        workhour.Hours__c = 9;
        workhour.User__c = user_id;
        insert workhour;
        //add to the list.
        workDays.add(workhour);
        
        //check it's calculate correctly.
        calcHours = ctrl.calcReportedAmountOfOurs(workDays);
        System.assertEquals(calcHours,9.0);


        //create another working hour and add it to the list and check that it's added to the calc Hours.
        Work_hours__c workhour2 = new Work_hours__c();
        workhour2.Project_name__c = project.Id;
        workhour2.Hours__c = 1.25;
        workhour2.User__c = user_id;
        insert workhour2;
        //add to the list
        workDays.add(workhour2);
        calcHours = ctrl.calcReportedAmountOfOurs(workDays);
        System.assertEquals(calcHours,10.25);
    }

    @isTest
    private static void testformatDate() {
        
        MessWorkingHoursController ctrl = new MessWorkingHoursController();
        
        Date testedDate = Date.newInstance(1960, 3, 17);
        String formatted = ctrl.formatDate(testedDate,'MMMM');
        System.assertEquals(formatted,'March');
    }
     @isTest
    private static void testWhDelete() {
           Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
    
            User usr = new User(LastName = 'TEST',
                FirstName='USER',
                Alias = 'tester',
                Email = 'User@sw.com',
                Username = 'User@sw.com',
                ProfileId = profileId.id,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US'
            );
    
            insert usr;
    
            //create controller instance.
            MessWorkingHoursController ctrl = new MessWorkingHoursController();
            ctrl.WorkHours.Date__c = Date.today();
            Id user_id = UserInfo.getUserId();
    
            //create working hour releated objects.
            Account acc = new Account(Name = 'Test Account');       
            insert acc;
            Contact con = new Contact(LastName = 'Test Contact',Account = acc,ServiceWise_Personnel__c = true,Birthdate = Date.today().addYears(-25));
            insert con;
            SFDC_Project__c project = new SFDC_Project__c(QA_is_required_for_all_project_issues__c='No',Name = 'Test Project',SFDC_Project_Manager__c = '005D0000001GgJE',Project_Type__c = 'Internal',Account__c = acc.Id); //This is Einat's user Id.
            insert project;
    
            Work_hours__c workhour = new Work_hours__c();
            workhour.Project_name__c = project.Id;
            workhour.Hours__c = 9;
            workhour.User__c = user_id;
            workhour.Date__c = Date.today();
            insert workhour;
            Delete workhour;
    }
}