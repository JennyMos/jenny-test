/****************************************************************************************
    Name              : LeadRegistrationDeleteBatch
    Description       : Delete lead registration records
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue  
    ----------------------------------------------------------------------------------------
    1. Dana (created)            24/02/2016               Dana                 [SW-17823]
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
global class LeadRegistrationDeleteBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{

global Database.Querylocator start(Database.BatchableContext BC){

        Date fromDate2Delete = system.today().adddays(-7);

        String q = 'select id, Create_lead__c from Lead_registration__c where Create_lead__c = true and createddate <=: fromDate2Delete';
        system.debug('quary = '+  q);
         
        return Database.getQueryLocator(q);
    }
    
    global void execute(Database.BatchableContext BC, List<Lead_registration__c> scope){
        try{
                Set<id> idRecords2Delete = new Set<id>();
                for(Lead_registration__c r : scope){
                  idRecords2Delete.add(r.Id);
                }
                
                Database.delete([SELECT Id FROM Lead_registration__c WHERE Id IN: idRecords2Delete],false);
                Database.emptyRecycleBin([SELECT Id FROM Lead_registration__c WHERE Id IN: idRecords2Delete ALL ROWS]);
        }
        catch(Exception e){
        }   
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}