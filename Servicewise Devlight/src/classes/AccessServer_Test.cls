/****************************************************************************************
    Name              : AccessServer_Test 
    Description       : 
    Revision History  :-
    Created/Modified by         Created/Modified Date     Requested by      Related Task/Issue      Description
    ----------------------------------------------------------------------------------------
    1. Gal (created)            11/12/2014               Dana             [SW-10678]                AuthorizeMyIP
    2. Gal                      24/03/2015               Dana             [SW-10678]                AccessServer_Schedulable
    ----------------------------------------------------------------------------------------    
    ****************************************************************************************/
@isTest
private class AccessServer_Test {

    static testMethod void AuthorizeMyIP() {
        Account acc = new Account(Name = 'test account', Enable_Authorize_My_IP__c = true);
        insert acc;
        Login_Details__c login = new Login_Details__c(Account__c = acc.Id, Username__c = 'username', Password__c = 'password', Environment__c = 'Salesforce Sandbox UAT', Enable_Authorize_My_IP__c = true);
        insert login;
        AccessServer controller = new AccessServer();
        PageReference pageRef = new Pagereference('/apex/AuthorizeMyIP');
        pageRef.getParameters().put('id', login.Id);
        Test.setCurrentPageReference(pageRef);
        controller.init();
        AccessServer.GetUserIPAddress();
        
        
    }
    
    static testMethod void sendEMailAlertTest() {
        Account acc = new Account(Name = 'test account', Enable_Authorize_My_IP__c = true);
        insert acc;
        Login_Details__c login = new Login_Details__c(Account__c = acc.Id, Username__c = 'username', Password__c = 'password', Environment__c = 'Salesforce Sandbox UAT', Enable_Authorize_My_IP__c = true);
        insert login;
        
    	AccessServer.sendAlertByEmail(login.Id,'1.2.3.4','TEST MESSAGE');   
    }
    
    static testMethod void AccessServer_Schedulable() {
        Datetime d = System.now().addSeconds(10);
        String cron = d.second() + ' ' + d.minute() + ' ' + d.hour() + ' ' + d.day() + ' ' + d.month() + ' ? ' + d.year();
        Account acc = new Account(Name = 'test account', Enable_Authorize_My_IP__c = true);
        insert acc;
        Login_Details__c loginDetails = new Login_Details__c(Account__c = acc.Id, Enable_Authorize_My_IP__c = true);
        insert loginDetails;
        test.startTest();
        String jobId = System.schedule('AccessServer_Schedulable_test', cron, new AccessServer_Schedulable(loginDetails.Id, '127.0.0.1'));
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
        NextFireTime
        FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals(cron, ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
        System.assertEquals(d, ct.NextFireTime);
        // Run scheduled task
        test.stopTest();
    }
}