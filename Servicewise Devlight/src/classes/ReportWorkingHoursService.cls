/************************************************************************************** 
Name              : ReportWorkingHoursService 
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Jenny	              30/03/2017               Dana                  [SW-23982]
****************************************************************************************/
global class ReportWorkingHoursService implements Messaging.InboundEmailHandler{
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
	   try{   
                string  htmlBody = '';
				String delimiter = ';';
                Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
                String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();  
                baseUrl=baseUrl.replace('http','https');    
                List<Work_hours__c> WorkingHoursToInsert = new List<Work_hours__c>();                
                List<WHEmailServicMapping__mdt> fieldsMapping = [select attachmentColumnHeader__c, fieldApiName__c from WHEmailServicMapping__mdt];
				Map<String, Schema.SObjectField> M = Schema.SObjectType.Work_hours__c.fields.getMap(); 
                Map<String,String> colName_apiName = new Map<String,String>();
				Map<String,Integer> colName_colIndex = new Map<String,Integer>();
                Set<String> ProjectIdsNotFound = new Set<String>();
				Map<Integer,List<String>> MapattachmentValues = new Map<Integer,List<String>>();
				String[] headerValues;
				User Finance = [Select Id From User Where Name = 'finance'];
				List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>();

				//check if name in subject exists in picklist values
				List<Schema.PicklistEntry> NamesPicklist = Work_hours__c.Subcontractor__c.getDescribe().getPicklistValues();
				Boolean nameFound = false;
				for(Schema.PicklistEntry f : NamesPicklist){
					if(f.getValue() == email.subject)
						nameFound = true;
				}
				if(!nameFound){
					sendErrorEmail('There is no existing name matching the name in the subject', email);
                    return null;
				}

                for(WHEmailServicMapping__mdt mapFld : fieldsMapping)
                {
                    String colNmae1 = mapFld.attachmentColumnHeader__c.toLowerCase();
                    String colNmae = colNmae1.trim();
                    colNmae = colNmae.replaceAll('(\\s+)', ' ');
              
                    colName_apiName.put(colNmae,mapFld.fieldApiName__c); 
                }  
				//check attachments validity
				if(email.textAttachments == null || email.textAttachments.size() < 1){
					sendErrorEmail('No attachment found in the email', email);
                    return null;
				}
				if(email.textAttachments[0].mimeTypeSubType != 'text/csv'){
					sendErrorEmail('There was a problem with the attachment format',email);
					return null;
				}
					
                if (email.textAttachments != null && email.textAttachments.size() > 0) {
                    WorkingHoursToInsert = new List<Work_hours__c>();                    
                    Messaging.InboundEmail.TextAttachment att = email.textAttachments[0];
                    string bodyContent = att.body; 
                    list<string> filelines = bodyContent.split('\n');   
					system.debug('filelines' + filelines);                       
                    headerValues = new String[]{};
                    headerValues = filelines[0].split(delimiter);
                    system.debug('headerValues = ' + headerValues);
                    Set<String> headerValuesFixed = new Set<String>();                                               
                    for (Integer x=0;x<headerValues.size();x++){                                                     
                          String fixedHeader = headerValues[x].toLowerCase();
                          fixedHeader = fixedHeader.trim();
                          fixedHeader = fixedHeader.replaceAll('(\\s+)', ' ');
                          fixedHeader = fixedHeader.remove('\"');
                          headerValuesFixed.add(fixedHeader);                     
                          if(colName_apiName.containsKey(fixedHeader))
                          {
                             colName_colIndex.put(fixedHeader,x);
                          }                         
                    } 
                    system.debug('colName_colIndex = ' + colName_colIndex);
                    system.debug('colName_apiName = ' + colName_apiName);
                    if(colName_colIndex.size() < 5){
						//Error and return
						sendErrorEmail('Attached Table Has Less then 5 columns or doesn\'t match criteria ' , email);
                        return null;
                    }
                   
                    //assign values from the document to relevant opportunities
                    for (Integer j=1;j<filelines.size();j++){   
                             String[] inputvalues = new String[]{};
                             inputvalues = filelines[j].split(delimiter,5);
                             Work_hours__c tmpWH = new Work_hours__c(); 
                             tmpWH.User__c = Finance.Id;
							 tmpWH.Subcontractor__c = email.subject;
							 List<String> linevalues =  new  List<String> ();
                             //for on document columns
                             for(String colHeader : colName_colIndex.keySet())
                             {   
								try{
									//get field api name and type according to column header
									Integer colIndx =  colName_colIndex.get(colHeader);
									String apiFieldName  = colName_apiName.get(colHeader);

									Schema.SObjectField field = M.get(apiFieldName);
									Schema.DescribeFieldResult F = field.getDescribe();
									Schema.DisplayType FldType = F.getType();
									Boolean isFieldreq  = !F.isNillable() ;// if false field is mandatory.

									String inputVal = inputvalues.get(colIndx).remove('\"');
									linevalues.add(inputVal);

									if(inputVal == '' && isFieldreq){
										sendErrorEmail('Required field ' + colHeader +' is missing in line number '+ j ,email);

										return null;
									}
									if(inputVal != ''){
										if(string.valueOf(FldType) == 'DOUBLE')//DOUBLE
													tmpWH.put(apiFieldName,DOUBLE.valueOf(inputVal));		
										else if(string.valueOf(FldType) == 'integer')
													tmpWH.put(apiFieldName,Integer.valueOf(inputVal));
										else if(string.valueOf(FldType) == 'Decimal')
													tmpWH.put(apiFieldName,Decimal.valueOf(inputVal));
										else if(string.valueOf(FldType) == 'DateTime')
													tmpWH.put(apiFieldName,DateTime.parse(inputVal));
										else if(string.valueOf(FldType) == 'Date' )
													tmpWH.put(apiFieldName,Date.parse(inputVal));
										else// For String and ID
												tmpWH.put(apiFieldName,inputVal); 
									}
								}catch(exception e){
										sendErrorEmail('Attached Table Has Illegal Values in line number '+ j + '. Error messsage: ' + e.getMessage(),email);
										system.debug('Error Cause='+ e.getCause() + ' Trace= ' + e.getStackTraceString() + ' Line NUmber= ' +e.getLineNumber() + ' Type is= ' + e.getTypeName() + ' Msg is= ' + e.getMessage() );
										return null;
									}       
								}
								//add line for values map
								MapattachmentValues.put(j,linevalues);
								WorkingHoursToInsert.add(tmpWH); 
                        }
                }
				System.debug('WorkingHoursToInsert:'+WorkingHoursToInsert); 
                System.debug('MapattachmentValues:'+MapattachmentValues); 
				//build result file
				Boolean failed =  false;
				String Content = ''; 
				//create header
				String Headernames = '';
				for(String colname :headerValues)
					Headernames += colname + delimiter;
				Headernames += 'Status ' + delimiter + 'Error message' + delimiter + 'Link \n';
				Content += Headernames;
				if( WorkingHoursToInsert != Null && WorkingHoursToInsert.size() > 0 ){
					Savepoint sp = Database.setSavepoint();
					Database.SaveResult[] resultList = DataBase.insert(WorkingHoursToInsert, false);
					System.debug('resultList:'+ resultList);
					//check if there were errors
					for (Integer sr=0;sr < resultList.size();sr++) {
						if(!resultList[sr].isSuccess())
							failed = true;
					}
					//create lines
					if(failed){
						Database.rollback(sp);
						for (Integer sr=0;sr < resultList.size();sr++) {
							for (String v:MapattachmentValues.get(sr+1))
								Content += v + delimiter;
							//success column
							Content += resultList[sr].isSuccess()?'Success':'Failer' + delimiter;
							//Error details column
							if(resultList[sr].isSuccess())
								Content += delimiter;
							else{
								String errors = '';
								String mess ='';
								for(Database.Error err : resultList[sr].getErrors()){
									mess = err.getMessage();
									if(mess.contains('Attempt to de-reference a null object'))
										mess = 'Required field is blank';
									errors += err.getStatusCode() + ': ' + mess;
								}
								Content += errors ;

							}
							Content += '\n';
						}
					}else{//all lines succeded
						for (Integer sr=0;sr < resultList.size();sr++){
							for (String v:MapattachmentValues.get(sr+1))
								Content += v + delimiter;
							Content += 'Success' + delimiter + '' + delimiter;
							Content += baseUrl+'/'+resultList[sr].getId()+ '\n';
						}
					}
					
					system.debug(Content);
					fileAttachments = new List<Messaging.EmailFileAttachment>();
					Messaging.EmailFileAttachment attatchToUpload = new Messaging.EmailFileAttachment();
					attatchToUpload.setBody(Blob.valueof(Content));
					attatchToUpload.setContentType('text/csv');
					attatchToUpload.setFileName('Results.csv');
					fileAttachments.add(attatchToUpload);
					sendSuccessErrorEmail(htmlBody,fileAttachments,email);
				}
            return result; 

        }catch(Exception e){
            system.debug('Error === '+ e.getMessage() + 'stackTrace = ' + e.getStackTraceString()); 
            return null;
        }  
    }
    
	private void sendErrorEmail(string htmlBody, Messaging.InboundEmail email){

        Messaging.EmailFileAttachment attatchToUpload = new Messaging.EmailFileAttachment();
		if(email.textAttachments != null ){
			attatchToUpload.setBody(Blob.valueof(email.textAttachments[0].body));
			attatchToUpload.setContentType(email.textAttachments[0].mimeTypeSubType);
			attatchToUpload.setFileName(email.textAttachments[0].fileName);
			List<Messaging.EmailFileAttachment> fileAttachments= new List<Messaging.EmailFileAttachment>{attatchToUpload};
			sendSuccessErrorEmail(htmlBody,fileAttachments,email);
		}else{
			sendSuccessErrorEmail(htmlBody,null,email);
		}
        system.debug( 'There is an error: '+ htmlBody);
	}

    private void sendSuccessErrorEmail(string htmlBody,List<Messaging.EmailFileAttachment> fileAttachments, Messaging.InboundEmail email){
        
        list<String> recipients = new list<string>{email.fromAddress};
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
       if(recipients.size() > 0){
            mail.setToAddresses(recipients);
            mail.setSenderDisplayName('Salesforce');
            mail.setSubject('Working Hours Insert');           
            mail.setHtmlBody(htmlBody);
			if(fileAttachments != null)        
				mail.setFileAttachments(fileAttachments);          
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
    }
}