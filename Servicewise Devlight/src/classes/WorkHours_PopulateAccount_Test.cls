/************************************************************************************** 
Name              : Account_UpdateBusinessHours_Test
Description       : Update Business Hours on all Entitlements From Account
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Irit                 06/11/2013              Yael Perry              [SW-6613]
****************************************************************************************/
@isTest
private class WorkHours_PopulateAccount_Test {

    static testMethod void PopulateAccountFromProject() 
    {
        List<Working_Hour_Standard__c> ls = Test.loadData(Working_Hour_Standard__c.sObjectType, 'WHStandardEx');
        /*User usr = new User(LastName = 'Test ProjectManager User',Alias = 'tpu',Email = 'TestPM@service-wise.com',Username = 'TestPM',CommunityNickname = 'TPM');
        insert usr;*/
        Account acc = new Account(Name = 'Test Account');       //,MigrationTest__c='77777777'
        insert acc;
        Contact con = new Contact(LastName = 'Test Contact',First_Name_Hebrew__c = 'בדיקה',Account = acc,ServiceWise_Personnel__c = true, Birthdate=System.today());
        insert con;
        SFDC_Project__c project = new SFDC_Project__c(Name = 'Test Project',SFDC_Project_Manager__c = '005D0000001GgJE',Project_Type__c = 'Pre-Sale',Account__c = acc.Id, Business_Unit__c='Enterprise', QA_is_required_for_all_project_issues__c ='No'); //This is Einat's user Id.
        insert project;
        SFDC_Issue__c issue = new SFDC_Issue__c(SFDC_Issue_Name__c = 'Test Issue',Issue_Type__c = 'Development / Implementation',SFDC_Issue_Priority__c = '3 - High',SFDC_Project__c = project.Id,Assigned_To__c = con.Id,Testing_Environment__c = 'Sandbox');        
        insert issue;
        Work_hours__c workhour = new Work_hours__c();
        workhour.Issue_Number__c = issue.Id;
        workhour.Project_name__c = project.Id;
        workhour.Hours__c = 9;
        workhour.User__c = UserInfo.getUserId();
        Test.startTest();
        insert workhour;
        update workhour; 
        Work_hours__c wh = [Select Id,Account__c,Project_name__c From Work_hours__c Where Id =: workhour.Id];       
        system.assertEquals(project.Account__c ,wh.Account__c);
        
        Test.stopTest();
    }
}