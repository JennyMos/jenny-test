// 03/05/12 Gal Kopfstein
trigger CalculateWorkingHoursOnIssue on Work_hours__c (after delete, after insert, after undelete, 
after update) {
    system.debug('--2--');
    if (TriggerSettings.IsTriggerActive('CalculateWorkingHoursOnIssue', 'CalculateWorkingHoursOnIssue')){//This Trigger Merged into class WorkhoursTriggerHandler 
    system.debug('---22222----');
            //FIRST SOLUTION
    
            List<Id> issues = new List<Id>();
            if (Trigger.isInsert || Trigger.isUnDelete)
            {
                for (Work_hours__c wh : Trigger.new) if (wh.Issue_Number__c != null && wh.Hours__c > 0) issues.add(wh.Issue_Number__c);
            }
            else if (Trigger.isUpdate) 
            {
                for (Work_hours__c wh : Trigger.new) 
                {
                    if (wh.Issue_Number__c != null) issues.add(wh.Issue_Number__c);
                    if (Trigger.oldMap.get(wh.Id).Issue_Number__c != null && Trigger.oldMap.get(wh.Id).Issue_Number__c != wh.Issue_Number__c) issues.add(Trigger.oldMap.get(wh.Id).Issue_Number__c);
                }
            }
            else if (Trigger.isDelete)
            {
                for (Work_hours__c wh : Trigger.old)
                {
                    if (wh.Issue_Number__c != null && wh.Hours__c > 0)
                    {
                        issues.add(wh.Issue_Number__c);
                    }
                }
            }
    
            AggregateResult[] groupedResults = [SELECT Issue_Number__c,SUM(Hours__c) FROM Work_hours__c where Issue_Number__c IN : issues GROUP BY Issue_Number__c];
            Map<Id,Decimal> actual_times = new Map<Id,Decimal>();
            for (AggregateResult groupedResult : groupedResults) actual_times.put((ID)groupedResult.get('Issue_Number__c'),(Decimal)groupedResult.get('expr0'));
    
            List <SFDC_Issue__c> updated_issues = [select Id,Actual_Time_Dev__c from SFDC_Issue__c where Id IN : actual_times.keySet()];
            for (SFDC_Issue__c updated_issue : updated_issues) updated_issue.Actual_Time_Dev__c = actual_times.get(updated_issue.Id);
    
            update updated_issues;
    
            //SECOND SOLUTION
    
            /*
            Map<Id,Decimal> WorkingHoursChange = new Map<Id,Decimal>();
    
            if (Trigger.isInsert || Trigger.isUnDelete)
            {
                for (Work_hours__c wh : Trigger.new)
                {
                    if (wh.Issue_Number__c != null && wh.Hours__c > 0)
                    {
                        if (WorkingHoursChange.containsKey(wh.Issue_Number__c)) WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c + WorkingHoursChange.get(wh.Issue_Number__c));
                        else WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c);
                    }
                }
            }
            else if (Trigger.isUpdate)
            {
                for (Work_hours__c wh : Trigger.new)
                {
                    if (wh.Issue_Number__c != null && Trigger.oldMap.get(wh.Id).Issue_Number__c != null && wh.Issue_Number__c == Trigger.oldMap.get(wh.Id).Issue_Number__c)
                    {
                        if (WorkingHoursChange.containsKey(wh.Issue_Number__c)) WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c - Trigger.oldMap.get(wh.Id).Hours__c + WorkingHoursChange.get(wh.Issue_Number__c));
                        else WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c - Trigger.oldMap.get(wh.Id).Hours__c);
                    }
                    else if (wh.Issue_Number__c != null && Trigger.oldMap.get(wh.Id).Issue_Number__c != null && wh.Issue_Number__c != Trigger.oldMap.get(wh.Id).Issue_Number__c)
                    {
                        if (WorkingHoursChange.containsKey(Trigger.oldMap.get(wh.Id).Issue_Number__c)) WorkingHoursChange.put(Trigger.oldMap.get(wh.Id).Issue_Number__c,WorkingHoursChange.get(wh.Issue_Number__c) - Trigger.oldMap.get(wh.Id).Hours__c);
                        else WorkingHoursChange.put(Trigger.oldMap.get(wh.Id).Issue_Number__c,- Trigger.oldMap.get(wh.Id).Hours__c);
                
                        if (WorkingHoursChange.containsKey(wh.Issue_Number__c)) WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c + WorkingHoursChange.get(wh.Issue_Number__c));
                        else WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c);
                    }
                    else if (wh.Issue_Number__c != null && Trigger.oldMap.get(wh.Id).Issue_Number__c == null)
                    {
                        if (WorkingHoursChange.containsKey(wh.Issue_Number__c)) WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c + WorkingHoursChange.get(wh.Issue_Number__c));
                        else WorkingHoursChange.put(wh.Issue_Number__c,wh.Hours__c);
                    }
                }
            }
            else if (Trigger.isDelete)
            {
                for (Work_hours__c wh : Trigger.old)
                {
                    if (wh.Issue_Number__c != null)
                    {
                        if (WorkingHoursChange.containsKey(wh.Issue_Number__c)) WorkingHoursChange.put(wh.Issue_Number__c,WorkingHoursChange.get(wh.Issue_Number__c) - wh.Hours__c);
                        else WorkingHoursChange.put(wh.Issue_Number__c,-wh.Hours__c);
                    }
                }
            }
    
            List<SFDC_Issue__c> issues = [select Id,Actual_Time_Dev__c from SFDC_Issue__c where Id IN : WorkingHoursChange.keySet()];
            for (SFDC_Issue__c issue : issues)
            {
                issue.Actual_Time_Dev__c +=  WorkingHoursChange.get(issue.Id);
            }
            update issues;
            */

  }
}