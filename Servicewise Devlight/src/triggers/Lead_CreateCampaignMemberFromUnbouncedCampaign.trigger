/**************************************************************************************
Name              : Lead_CreateCampaignMemberFromUnbouncedCampaign
Description       : Create campaign member with: LeadId, Status='Registered', and Unbounced CampaignId.
Revision History  :-
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Irit                 22/07/2013              Mari Schuller          [SW-5663]
****************************************************************************************/
trigger Lead_CreateCampaignMemberFromUnbouncedCampaign on Lead (after insert) 
{
	system.debug('>>> Start Lead_CreateCampaignMemberFromUnbouncedCampaign');
	Set<Id> campaignSet = new Set<Id>();
    try
    {
        for(integer i = 0; i < trigger.size; i++)
        {        	        	
            if(String.isNotBlank(trigger.new[i].Unbounced_Campaign_ID__c))
            {
                campaignSet.add(trigger.new[i].Unbounced_Campaign_ID__c);                
            }        	   
        }
		if(!campaignSet.isEmpty())
		{
			Map<Id,Campaign> campaignMap = new Map<Id,Campaign>([Select Id,Name From Campaign Where Id IN: campaignSet]);
			if(!campaignMap.isEmpty())
			{
				List<CampaignMember> cmList = new List<CampaignMember>();
				for(Lead l: trigger.new)
				{
					if(campaignMap.containsKey(l.Unbounced_Campaign_ID__c))
					{
						cmList.add(new CampaignMember(LeadId = l.Id,CampaignId = l.Unbounced_Campaign_ID__c,Status = 'Registered'));																	
					}
				}
				if(!cmList.isEmpty())
				{
					insert cmList;
					system.debug('>>> Create CM');
				}
			}
		}
		
	}
	catch(Exception e)
    {
        trigger.new[0].addError(e.getMessage());
    }  
}