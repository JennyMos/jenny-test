/************************************************************************************** 
Name              : WorkHours_PopulateAccountFromProject
Description       : Update Account field on Work_hours__c from Project lookup
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Irit                 06/11/2013              Yael Perry           [SW-6613]
****************************************************************************************/
trigger WorkHours_PopulateAccountFromProject on Work_hours__c (before insert, before update) 
{         system.debug('---1---');
     if (TriggerSettings.IsTriggerActive('WorkHours_PopulateAccountFromProject', 'WorkHours_PopulateAccountFromProject')){ //This Trigger Merged into class WorkhoursTriggerHandler 
            system.debug('---11111---');
            Set<Id> projIdSet = new Set<Id>();
            Map<Id,SFDC_Project__c> projMap = new Map<Id,SFDC_Project__c>();
    
            for(Work_hours__c wh: Trigger.new)        
            {                                                                                                                     
                if(wh.Project_name__c != null)
                {
                    projIdSet.add(wh.Project_name__c);  
                }
            }                                                          
                
            if(!projIdSet.isEmpty())
            {   
                projMap = new Map<Id,SFDC_Project__c>([select Id,Account__c from SFDC_Project__c where id IN : projIdSet]); 
                if(!projMap.isEmpty())
                {
                    for(Work_hours__c wh: Trigger.new) 
                    {
                        if(projMap.containsKey(wh.Project_name__c))
                        {
                            wh.Account__c = projMap.get(wh.Project_name__c).Account__c;
                            system.debug('Project: '+wh.Project_name__c + ' Account: '+ wh.Account__c);
                        }                         
                    }
                }
            }  
    }           
}