/************************************************************************************** 
Name              : WorkhoursTrigger
Description       : One location to trigger all the triggers related to Workhours object
Revision History  : -
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mendy               21/08/2014      			    Dana          		[SW-10006]
****************************************************************************************/
trigger WorkhoursTrigger on Work_hours__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
	WorkhoursTriggerHandler handler = new WorkhoursTriggerHandler();  
	// ------------------------------------------------------------------------
	//  ---------------------------- AFTER EVENTS -----------------------------
	// ------------------------------------------------------------------------
	
	if (Trigger.isBefore && Trigger.isInsert) // Before Insert
	{
			 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'AbsenceReasonValidation'))
				handler.AbsenceReasonValidation(trigger.new);

			 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'WorkHours_PopulateAccountFromProject'))
				handler.WorkHours_PopulateAccountFromProject(trigger.new);

			  if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'PreventReportingWH_AfterTheEndOfMonth'))
				 handler.PreventReportingWH_AfterTheEndOfMonth(trigger.new,null);
	}
	else if (Trigger.isBefore && Trigger.isUpdate) // Before Update
	{	
			if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'AbsenceReasonValidation'))
				handler.AbsenceReasonValidation(trigger.new);

			if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'WorkHours_PopulateAccountFromProject'))
				handler.WorkHours_PopulateAccountFromProject(trigger.new);

			if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'PreventReportingWH_AfterTheEndOfMonth'))
				 handler.PreventReportingWH_AfterTheEndOfMonth(trigger.new, trigger.oldMap);
	}
	/*else if (Trigger.isBefore && Trigger.isDelete) // Before Delete
	{
	}
	// ------------------------------------------------------------------------
	//  ---------------------------- AFTER EVENTS -----------------------------
	// ------------------------------------------------------------------------
	
	//else */
	else if (Trigger.isAfter && Trigger.isInsert) // After Insert
	{
		 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'CalculateWorkingHoursOnIssue'))
			handler.CalculateWorkingHoursOnIssue(trigger.new, trigger.old, trigger.oldMap);
	}
	else if (Trigger.isAfter && Trigger.isUpdate) // After Update
	{
		 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'CalculateWorkingHoursOnIssue'))
		  handler.CalculateWorkingHoursOnIssue(trigger.new, trigger.old, trigger.oldMap);
	}
	else if (Trigger.isAfter && Trigger.isDelete) // After Delete
	{
		 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'CalculateWorkingHoursOnIssue'))
			handler.CalculateWorkingHoursOnIssue(trigger.new, trigger.old, trigger.oldMap);
	}
	else if (Trigger.isAfter && Trigger.isUnDelete) // After UnDelete
	{
		 if (TriggerSettings.IsTriggerActive('WorkhoursTriggerHandler', 'CalculateWorkingHoursOnIssue'))
			handler.CalculateWorkingHoursOnIssue(trigger.new, trigger.old, trigger.oldMap);
	}
	
}