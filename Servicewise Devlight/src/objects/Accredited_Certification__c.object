<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Follow up on the certification of ServiceWise employees</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Certification__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Certification</label>
        <referenceTo>Certification__c</referenceTo>
        <relationshipLabel>Accredited certifications</relationshipLabel>
        <relationshipName>Accredited_certifications</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Completion_Date__c</fullName>
        <description>The date this certification was acquired by the employee</description>
        <externalId>false</externalId>
        <label>Completion Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.ServiceWise_Personnel__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Accredited certifications</relationshipLabel>
        <relationshipName>Accredited_certifications</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Help_Field__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Help filed for the PB &quot;Update Next Renewal Planned Date- From Renewal&quot;</description>
        <externalId>false</externalId>
        <label>Help Field</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Internal_Certification__c</fullName>
        <externalId>false</externalId>
        <formula>Certification__r.Internal_Certification__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Internal Certification</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Last_Time_The_Exam_Was_Taken__c</fullName>
        <description>Gives the date of the last time the exam was taken, for use in the renewal Email Alert</description>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Renewal_Date__c), Completion_Date__c,Renewal_Date__c)</formula>
        <label>Last Time The Exam Was Taken</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Next_Renewal_Planned_Date__c</fullName>
        <description>The date this certification needs to be taken again</description>
        <externalId>false</externalId>
        <label>Next Renewal Planned Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Relevant__c</fullName>
        <defaultValue>true</defaultValue>
        <description>If this certification is still relevant</description>
        <externalId>false</externalId>
        <label>Relevant</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Renewal_Date__c</fullName>
        <description>Last time this certification was to be taken</description>
        <externalId>false</externalId>
        <label>Renewal Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Accredited Certification</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Accredited Certification Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Accredited Certifications</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <visibility>Public</visibility>
</CustomObject>
