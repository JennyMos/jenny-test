({

	 loadjs:function(component,event){
         var compEvent = component.getEvent("load");
         var wh = component.get("v.wh");
		 console.log(wh);
         compEvent.setParams({"wh" : wh});
         compEvent.fire();
		      
    },
     saveOnchange:function (component,event,helper){
		var wh = component.get("v.wh");
		//console.log('Hours: '+ wh.Hours);
        if(wh.Hours == null)
            wh.Hours=0;
        if((wh.Hours == 0 || wh.Hours== null) && wh.WorkingHourId != '' && event.getParam("oldValue") != null){
            var answer = confirm("Once you press Save this working hour will be deleted. Are you sure you would like to delete this working hour?");
            if(!answer){
                helper.undohelper(component);
            }
        }
        var compEvent = component.getEvent("savonchange");
        compEvent.setParams({"whchange" : wh});
        compEvent.fire();
    },
    undo:function(component,event,helper){
   		helper.undohelper(component);
        var wh = component.get("v.wh");
        var compEvent = component.getEvent("savonchange");
        compEvent.setParams({"whchange" : wh});
        compEvent.fire();
    }
})