({
	getProjects : function(component,AccountId) {
		var callGetRelatedProjects = component.get("c.GetRelatedProjects");
        callGetRelatedProjects.setParams({AccountId:AccountId});
        callGetRelatedProjects.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var ProjectsMap = a.getReturnValue();
				var opts = [];
                for(var key in ProjectsMap)
                   opts.push({class: "optionClass", label: key, value:  ProjectsMap[key] }); 
                component.find("InputSelectDynamicProjects").set("v.options", opts);
                component.set("v.AccountToAdd",AccountId);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message);
            }
        });
		$A.enqueueAction(callGetRelatedProjects);
	},
    
    closepopup: function(component){
        component.set("v.AccountToAdd",'');
        component.set("v.ProjectToAdd",'');
		document.getElementById("backGroundSectionId").style.display = "none";
		document.getElementById("popupProject").style.display = "none";
    }
})