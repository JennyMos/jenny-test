({
    doInit: function(component, event, helper){
        var callGetallAccounts= component.get("c.GetallAccounts");
        callGetallAccounts.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ){
                var AccountsMap = a.getReturnValue();
				var opts = [];
                for(var key in AccountsMap)
                   opts.push({class: "optionClass", label: key, value:  AccountsMap[key] }); 
                component.find("InputSelectDynamicAccounts").set("v.options", opts);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(callGetallAccounts);
    },
    
	addProjectjs : function(component, event, helper) {
        var PId = component.get("v.ProjectToAdd");
        if(PId != ''){
            var compEvent = component.getEvent("addProjectEvent");
         	compEvent.setParams({"ProjectToAdd" : PId});
         	compEvent.fire();
        }else
            alert('Please choose a Project');
        helper.closepopup(component);
	},
    
    disablePopup : function(component, event, helper) {
        helper.closepopup(component);
	},
    
    refreshProjects : function(component, event, helper) {
        
        var Acc2send ='';
        var params = event.getParam('arguments');
        if (params) 
            Acc2send = params.AccId;
        else
            Acc2send = component.get("v.AccountToAdd");
        console.log('accountId in refresh'+ Acc2send);
		helper.getProjects(component,Acc2send);
	}
})