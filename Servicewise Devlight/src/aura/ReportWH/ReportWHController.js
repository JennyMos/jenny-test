({
	myAction : function(component, event, helper) {
		var issue = new sforce.SObject("SFDC_Issue__c"); 
        issue.Id = '{! SFDC_Issue__c.Id }'; 
        var th; 
        var tm; 
        var TimerHours; 
    
        /*calculating timer hours*/ 
        if('{!SFDC_Issue__c.Timer_Hours__c}' != ""){ 
            var y = '{!SFDC_Issue__c.Timer_Hours__c}'; 
            if (y%1==0) 
            { 
                th = y; 
                tm = 0; 
            } 
            else if (y > 10) 
            { 
                th = y.substring(0,2); 
                tm = y.substring(3,5); 
            } 
            else if (y > 1) 
            { 
                th = y.substring(0,1); 
                tm = y.substring(2,4); 
            } 
            else if(y>0) 
            { 
                th=0; 
                tm=y.substring(2,4); 
            } 
            else { 
                th=0; 
                tm=0; 
            } 
            
            if ((y*10)%1==0) 
            { 
                tm=tm*10;} 
                th = parseInt(th); 
                tm = parseInt(tm); 
                TimerHours = th+(tm/100); 
                TimerHours = Math.round(TimerHours*100)/100; 
            } 
        else { 
            TimerHours=0; 
        } 
        
        /*splitting start date*/ 
        var x = '{!SFDC_Issue__c.Start_Time__c}'; 
        var hour= x.substring(0,2); 
        var min= x.substring(3,5); 
        var day= x.substring(6,8); 
        var month= x.substring(9,11); 
        var year= x.substring(12,16); 
        /*calculating hours between dates*/ 
        var now = new Date(); 
        var nowMilliseconds = now.getTime(); 
        var newDt = new Date(year, month-1, day, hour, min, 0, 0); 
        var SDMilliseconds = newDt.getTime(); 
        var diff_ms = nowMilliseconds - SDMilliseconds; 
        var diff = diff_ms/(1000*60*60); 
        diff = Math.round(diff*100)/100; 
        
        if ('{!SFDC_Issue__c.Start_Time__c}'!= '') 
        { 
            if(diff > 12) 
            { 
                alert("You forgot to press Stop. Your last reporting hours were delete. Please add them manually."); 
                issue.Start_Time__c = null;	
            } 
            else{ 
                issue.Timer_Hours__c = TimerHours + diff; 
                issue.Start_Time__c = null; 
            } 
        } 
        var result = sforce.connection.update([issue]); 
        if(result[0].getBoolean("success")) 
        { 
            window.location = 'apex/Edit_Report_WH?scontrolCaching=1&id='+issue.Id; 
        } 
        else{ 
            alert('Error : '+result); 
            window.location.reload(); 
        }
    }
})