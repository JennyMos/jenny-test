({
	initProjectLine : function(cmp,event, helper){
	
	},
	openDialog: function (component, event, helper) {
		var proj = component.get("v.proj");
		var compEvent = component.getEvent("addProjectbyAccountEvent");
		console.log("AccountId in Project line:" + proj.AccountId);
		compEvent.setParams({"AccountId" : proj.AccountId});
		compEvent.fire();
	},
    addline: function (component, event, helper) {
        var proj = component.get("v.proj");
		var compEvent = component.getEvent("addProjectEvent");
        compEvent.setParams({"ProjectToAdd" : proj.ProjectId});
		compEvent.fire();
    },

	navigate:  function (component, event, helper) {
		var proj = component.get("v.proj");
		var wh = component.get("v.WorkHours");

		var Ev = $A.get("e.c:ReportRecurrentJobWHEvent");
        Ev.setParams({"projectId" : proj.ProjectId});
		Ev.setParams({"date" : wh.Date__c.toString()});
		Ev.fire();

		document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById("RecurentJob").style.display = "block"; 

	}
})