({
	doInit :function(component, event, helper) {
		component.set("v.wh", {'sObjectType':'Work_hours__c'});
		helper.SetPicklists(component);
	},

	Cancelctr : function(component, event, helper) {
		component.set("v.proj",'');
        component.set("v.wh",{'sObjectType':'Work_hours__c'});
        component.set("v.allWeek",false);
        var Eve = $A.get("e.c:CloseReportReccurentJobs");
        Eve.fire();
	},

	Savectr : function(component, event, helper) {
       
		var wh = component.get("v.wh");
        var isDaysOff = component.get("v.isDayOff");
		if(wh.To_Date__c < wh.From_Date__c)
			component.set("v.PageMessage",'To Date must be greater than From Date');
        else if(wh.From_Date__c == undefined || wh.To_Date__c == undefined || ((wh.AbsenceReason__c == undefined  || wh.AbsenceReason__c == '--Select--') && isDaysOff) || ((wh.Customer_Description__c == undefined || wh.Customer_Description__c == '--Select--') && !isDaysOff) || wh.Hours__c == undefined )
            component.set("v.PageMessage",'Please fill in all the required fields');
        else{
            component.set("v.PageMessage",'');
			helper.Savewh(component);
        }
	},

	setDays: function(component, event, helper) {
        
		if(component.get("v.allWeek")){
			var d = component.get("v.Date");
			helper.callSetDays(component,d );
		}
	},

	assignValues: function(component, event, helper) {
		 var projectId = event.getParam("projectId");
		 var date = event.getParam("date");
		 component.set("v.Date", date);
		 helper.getProj(projectId, component);
		 helper.checkDaysOff(projectId, component);
	}
})