({
	Savewh : function(cmp) {
        
        var Savejs= cmp.get("c.SaveCtr");
        var wh = cmp.get("v.wh");
        Savejs.setParams({whjson:JSON.stringify(wh)});
        Savejs.setCallback(this, function(a) {
            if (cmp.isValid() && a.getState() === "SUCCESS" ) {
                if( a.getReturnValue() == 'success'){
                    cmp.set("v.proj",'');
                    cmp.set("v.wh",{'sObjectType':'Work_hours__c'});
                    cmp.set("v.allWeek",false);
                    var Eve = $A.get("e.c:CloseReportReccurentJobs");
                    Eve.fire();
                }else{
                   cmp.set("v.PageMessage",a.getReturnValue()); 
                }  
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(Savejs);
		
	},
	getProj : function(projectId,cmp) {
		var wh = cmp.get("v.wh");
		var GetProjectDetails= cmp.get("c.getProjectName");
        GetProjectDetails.setParams({projectId:projectId });
        GetProjectDetails.setCallback(this, function(a) {
            if (cmp.isValid() && a.getState() === "SUCCESS" ) {
				var P = a.getReturnValue();
				wh.Project_name__c = P.Id;
				wh.Account__c = P.Account__c;
                cmp.set("v.proj",P);
			    cmp.set("v.wh",wh);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetProjectDetails);
	},
	checkDaysOff : function(projectId,cmp) {
		var GetProjectDetails= cmp.get("c.CheckIfIsDayOffProj");
        GetProjectDetails.setParams({projectId:projectId });
        GetProjectDetails.setCallback(this, function(a) {
            if (cmp.isValid() && a.getState() === "SUCCESS" ) {
				var P = a.getReturnValue();
				cmp.set("v.isDayOff",P);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetProjectDetails);
	},
    
	callSetDays : function(cmp,d) {
		var GetAllweek= cmp.get("c.SetDays");
        var wh = cmp.get("v.wh");
        GetAllweek.setParams({whjson:JSON.stringify(wh), dateStr: d});
        GetAllweek.setCallback(this, function(a) {
            if (cmp.isValid() && a.getState() === "SUCCESS" ) {
                debugger;
				var resultList = a.getReturnValue();
                wh.From_Date__c = resultList[0];
                wh.To_Date__c = resultList[1];
                cmp.set("v.wh",wh);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetAllweek);
	},

	SetPicklists:function(component){
        var GetAbsenswReasons= component.get("c.GetPickListValue");
        GetAbsenswReasons.setParams({objectType:'Work_hours__c', 
                                    picklistName:'AbsenceReason__c',
                                    hasNoneValue:true
                                   });
        GetAbsenswReasons.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var StrList = a.getReturnValue();
               component.set("v.AbsenceReasons",StrList);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetAbsenswReasons);
        var GetTravelWay= component.get("c.GetPickListValue");
        GetTravelWay.setParams({objectType:'Work_hours__c', 
                                    picklistName:'Customer_Description__c',
                                    hasNoneValue:true
                                   });
        GetTravelWay.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var StrList2 = a.getReturnValue();
				component.set("v.CustomerDescriptions",StrList2);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetTravelWay);
	}
})