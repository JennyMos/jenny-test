({  

    addProjecthelper: function(component,ProjectToAdd){
        var calladdProject= component.get("c.addProject");
        var Projects = JSON.stringify(component.get("v.allProjects"));
		var wh = component.get("v.WorkHours");
        calladdProject.setParams({jsonallProjects:Projects
                                  ,ProjectToAdd:ProjectToAdd,Date4WH:wh.Date__c});
        calladdProject.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var Projects = a.getReturnValue();
                component.set("v.allProjects",Projects);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(calladdProject);
    },
    
    calcTotalPerDay:function(component){
        var Projects = JSON.stringify(component.get("v.allProjects"));
    	var calcTotalPerDay = component.get("c.calcTotalPerDay");
        calcTotalPerDay.setParams({ jsonallProj:Projects});
        calcTotalPerDay.setCallback(this, function(response2) {
        	if (response2.getState() === "SUCCESS")
               component.set("v.Total",response2.getReturnValue());
            else
               console.log("Error message: "+response2.getError());                      
        });
        $A.enqueueAction(calcTotalPerDay);
	},
    SetPicklists:function(component){
        var GetAbsenswReasons= component.get("c.GetPickListValue");
        GetAbsenswReasons.setParams({objectType:'Work_hours__c', 
                                    picklistName:'AbsenceReason__c',
                                    hasNoneValue:true
                                   });
        GetAbsenswReasons.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var StrList = a.getReturnValue();
               component.set("v.AbsenceReasons",StrList);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetAbsenswReasons);
        var GetTravelWay= component.get("c.GetPickListValue");
        GetTravelWay.setParams({objectType:'Work_hours__c', 
                                    picklistName:'Travel_way__c',
                                    hasNoneValue:true
                                   });
        GetTravelWay.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var StrList2 = a.getReturnValue();
				component.set("v.TravelWays",StrList2);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetTravelWay);
		var GetCustomerDescription= component.get("c.GetPickListValue");
        GetCustomerDescription.setParams({objectType:'Work_hours__c', 
                                    picklistName:'Customer_Description__c',
                                    hasNoneValue:true
                                   });
        GetCustomerDescription.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var StrList3 = a.getReturnValue();
				component.set("v.CustomerDescriptions",StrList3);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetCustomerDescription);
    },

    HelperGetRelatedIssues:function(component,projId,issueId){
        var GetRelatedIssuesFromApex = component.get("c.GetRelatedIssues");
        var obj = component.get("v.onlyOpenIssue");
        console.log('issueId: '+ issueId);
        GetRelatedIssuesFromApex.setParams({ProjectId:projId, OnlyOpen:obj, thisIssue: issueId});
        GetRelatedIssuesFromApex.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS" ) {
                var IssueMap = a.getReturnValue();
				var opts = [];
                debugger;
                for(var key in IssueMap){
                    if(issueId == IssueMap[key] )
                        opts.push({class: "optionClass", label: key, value:  IssueMap[key], selected: "true" }); 
                    else 
                        opts.push({class: "optionClass", label: key, value:  IssueMap[key] });
                }
               var selectlist = component.find("InputSelectDynamic");
               selectlist.set("v.options", opts);
            }else if (a.getState()  === "ERROR"){
                 var errors = a.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message); 
            }
        });
		$A.enqueueAction(GetRelatedIssuesFromApex);
    },
    reCalc:function(component){
        var reCalc = component.get("c.reCalc");
        var Projects = JSON.stringify(component.get("v.allProjects"));
        reCalc.setParams({jsonallProjects:Projects});
        reCalc.setCallback(this, function(response) {
        	if (response.getState() === "SUCCESS") {
             	component.set("v.allProjects",response.getReturnValue());
                /*********calculate all totals per day******************/
               this.calcTotalPerDay(component);
            }else{
                 var errors = response.getError();
                 if (errors[0] && errors[0].message) 
                     console.log("Error message: " + errors[0].message);
            }
          });
     $A.enqueueAction(reCalc);                       
    },
   
	SetDaysOfWeekDates:function(component){
        
        var SetDaysOfWeekDates = component.get("c.SetDaysOfWeekDates");  
        SetDaysOfWeekDates.setParams({
            TodayDay:component.get("v.WorkHours.Date__c") 
        });
        SetDaysOfWeekDates.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	var datelist = a.getReturnValue();
             	component.set("v.SundayDate",datelist[0]);
            	component.set("v.MondayDate",datelist[1]);
            	component.set("v.TuesdayDate",datelist[2]);
            	component.set("v.WednesdayDate",datelist[3]);
            	component.set("v.ThursdayDate",datelist[4]);
            	component.set("v.FridayDate",datelist[5]);
            	component.set("v.SaturdayDate",datelist[6]);
                this.clearSelected(component);
                console.log('clear SelectedWH in SetDaysOfWeekDates');
            }else if (a.getState() === "ERROR"){
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(SetDaysOfWeekDates);    
    },
    
    SetProjects:function(component){ 
        var SetProjects = component.get("c.setProjects");
        SetProjects.setParams({
            Date4WH:component.get("v.WorkHours.Date__c") 
        });
		console.log(component.get("v.WorkHours.Date__c") );
        SetProjects.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	component.set("v.allProjects", a.getReturnValue());
                /*********calculate all totals per day******************/
                this.calcTotalPerDay(component);
                this.clearSelected(component);
                console.log('clear SelectedWH in SetProjects');
                component.set("v.PageMessage",''); 
            }else if (a.getState() === "ERROR"){
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                 }
        	});
        $A.enqueueAction(SetProjects);
    },

	clearSelected:function(component){

		var wh = component.get("v.WorkHours");
		component.set("v.SelectedWH",{class: "WorkingHourWrapper",isCompleted:"false"});
		wh.Issue_Number__c = null;
		wh.Project_name__c = null;
		component.set("v.WorkHours",wh);
	},
        
    checkValidationshelper:function(component){
       var allProjects = JSON.stringify(component.get("v.allProjects"));
       var callcheckValidations = component.get("c.checkValidations");
       var noError = true;
       callcheckValidations.setParams({jsonallProjects:allProjects});
       return new Promise(function(resolve, reject) {
           callcheckValidations.setCallback(this, function(response){
                if (response.getState() === "SUCCESS") {
                    allProjects = response.getReturnValue();                    
                    for(var idx in allProjects)
                        if(allProjects[idx].Error )
                            noError = false;
                    if(!noError){
                        component.set("v.allProjects",allProjects);
                        component.set("v.PageMessage",'Please review the errors in the red working hours. Your data wasn\'t saved');
						var selected = component.get("v.SelectedWH");
						for(var idx in allProjects){
							for(var idx1 in allProjects[idx].firstLineAllWeekWH){
								if(allProjects[idx].firstLineAllWeekWH[idx1].Project == selected.Project && allProjects[idx].firstLineAllWeekWH[idx1].WHID == selected.WHID)
									component.set("v.SelectedWH", allProjects[idx].firstLineAllWeekWH[idx1]);
							}
							for(var idx2 in allProjects[idx].restAllWeekWH){
								for(var idx3 in allProjects[idx].restAllWeekWH[idx2]){
									if(allProjects[idx].restAllWeekWH[idx2][idx3].Project == selected.Project && allProjects[idx].restAllWeekWH[idx2][idx3].WHID == selected.WHID)
									component.set("v.SelectedWH", allProjects[idx].restAllWeekWH[idx2][idx3] );
								}
							}        
						}
                 		reject(Error("One of the wh you tried to update is invalid"));
                    }
                    else
                        resolve("No errors");
                }else{
                     var errors = response.getError();
                     if (errors[0] && errors[0].message) 
                         reject(Error("Database Error message: " + errors[0].message));
                }
              });
       		$A.enqueueAction(callcheckValidations);
       });   
    }
})