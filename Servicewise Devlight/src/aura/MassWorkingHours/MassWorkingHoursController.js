({
	 doInit: function(component, event, helper){
       //var parent_url = "servicewise--devlight--c.cs81.visual.force.com/apex/ReportRecurrentJobWH?date=2017-07-24&projectId=a0A26000001kg07EAA";
	   //component.set("v.vfHost",parent_url);
        var getWorkHours = component.get("c.getWorkHours");
        getWorkHours.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS") {
                component.set("v.WorkHours",a.getReturnValue());
        		helper.SetPicklists(component);
            }
            else if (a.getState() === "ERROR"){
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        	});
        $A.enqueueAction(getWorkHours);
        var getUserName = component.get("c.getUserName");
        getUserName.setCallback(this, function(a) {
            if (component.isValid() && a.getState() === "SUCCESS") {
                component.set("v.CurrentUser",a.getReturnValue());
            }
        	});
		$A.enqueueAction(getUserName);
    },
    
    CloseRepRecDualog:function(component, event, helper){
        debugger;
        helper.SetProjects(component);
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById("RecurentJob").style.display = "none";
    },
    
    ConfirmWorkingHours:function(component, event, helper){
        var currentDate = component.get("v.WorkHours").Date__c;
        console.log('currentDate: '+ currentDate);
        var getExpectedAndReported = component.get("c.getExpectedAndReportedapex");
        getExpectedAndReported.setParams({cDate:currentDate});
        getExpectedAndReported.setCallback(this, function(a){
            if(component.isValid() && a.getState() === "SUCCESS"){
                var result = a.getReturnValue();
                if(result[1] >= result[0])
                    alert("You reported enough working hours for this month.");
                else
					alert("You didn't report "  + (result[0] - result[1]) +  " hours as expected for this month.");
            }else if (a.getState() === "ERROR"){
                    var errors = a.getError();
                    if (errors)
                        if (errors[0] && errors[0].message)
                            console.log("Error message: " + errors[0].message);
                    else
                        console.log("Unknown error");
            }
        });
        $A.enqueueAction(getExpectedAndReported);
        
    },
    GetProjectsFromLastWeek:function(component, event, helper){
        helper.clearSelected(component);
		 component.set("v.PageMessage",'');
        console.log('clear SelectedWH in GetProjectsFromLastWeek');
		var wh = component.get("v.WorkHours");
        var Projects = JSON.stringify(component.get("v.allProjects"));
        var callProjectsFromLastWeek = component.get("c.GetProjectsFromLastWeekapex");
        callProjectsFromLastWeek.setParams({jsonallProjects:Projects,Date4WH:wh.Date__c});
        callProjectsFromLastWeek.setCallback(this, function(a) {
                if (component.isValid() && a.getState() === "SUCCESS") {
                    component.set("v.allProjects",a.getReturnValue());
                }else{
                      var errors = a.getError();
                      if (errors[0] && errors[0].message) 
                         console.log("Error message: " + errors[0].message); 
                }
        	});
			$A.enqueueAction(callProjectsFromLastWeek);
    },
    
    SaveDatajs: function(component, event, helper){
        component.set("v.PageMessage",'');
        var ValidationPromise = helper.checkValidationshelper(component);
		var wh = component.get("v.WorkHours");
		ValidationPromise.then(
        	$A.getCallback(function(result){
                var Projects = JSON.stringify(component.get("v.allProjects"));
                var callSaveData = component.get("c.SaveDataapex");
                    callSaveData.setParams({jsonallProjects:Projects,Date4WH:wh.Date__c});
                    callSaveData.setCallback(this, function(a) {
                        if (component.isValid() && a.getState() === "SUCCESS") {
                            var updated = a.getReturnValue();
                            if(updated != null){
                                component.set("v.allProjects",updated);
                                component.set("v.PageMessage",'Your working hours were saved successfully!');
								component.find("globalmessage").set("v.severity","confirm");
                                helper.clearSelected(component);
                            }else
								component.set("v.PageMessage",'Error occurred');
                        }else{
                              var errors = a.getError();
                              if (errors[0] && errors[0].message) 
                                 console.log("Error message: " + errors[0].message); 
                        }
                    });
                 $A.enqueueAction(callSaveData);
            }),
            $A.getCallback(function(error){
            	console.log('An error occurred : ' + error.message);
        	})
        );
    },
    
    changedDate: function(component, event, helper){
        
        if(event.getParam("oldValue") != event.getParam("value")){
            console.log('Date was updated');
            var ValidationPromise = helper.checkValidationshelper(component);
            ValidationPromise.then(
                $A.getCallback(function(result){
                   helper.SetProjects(component);
                   helper.SetDaysOfWeekDates(component);
                }),
                $A.getCallback(function(error){
                    console.log('An error occurred : ' + error.message);
                })
             );
        }
    },
    
    OnSelectedChange: function(component,event,helper){
    	var allProjects = component.get("v.allProjects");
        var selected = component.get("v.SelectedWH");

		var wh = component.get("v.WorkHours");
		selected.IssueNumber = wh.Issue_Number__c;
        for(var idx in allProjects){
            for(var idx1 in allProjects[idx].firstLineAllWeekWH){
                if(allProjects[idx].firstLineAllWeekWH[idx1].Project == selected.Project && allProjects[idx].firstLineAllWeekWH[idx1].WHID == selected.WHID)
                    allProjects[idx].firstLineAllWeekWH[idx1] = selected;
            }
            for(var idx2 in allProjects[idx].restAllWeekWH){
                for(var idx3 in allProjects[idx].restAllWeekWH[idx2]){
                    if(allProjects[idx].restAllWeekWH[idx2][idx3].Project == selected.Project && allProjects[idx].restAllWeekWH[idx2][idx3].WHID == selected.WHID)
                    allProjects[idx].restAllWeekWH[idx2][idx3] = selected;
                }
            }        
        }
        component.set("v.allProjects",allProjects);
    },
    
    refresh: function (component,event,helper){
        var answer = confirm("Are you sure you would like to refresh the table? All unsaved changed will be deleted.");
        if(answer)
            helper.SetProjects(component);
    },
    
    load:function(component, event,helper){
		debugger;
        component.set("v.onlyOpenIssue",true);
        var currentwh = event.getParam("wh");
		var ifDaysoff = component.get("c.ifDaysoffProject");
        ifDaysoff.setParams({ProjId:currentwh.Project});
        ifDaysoff.setCallback(this, function(a) {
               if (component.isValid() && a.getState() === "SUCCESS") {
					component.set("v.SelectedWH",currentwh);
					if(a.getReturnValue() == false)
						helper.HelperGetRelatedIssues(component,currentwh.Project,currentwh.IssueNumber);
					component.set("v.IsDayOffProject",a.getReturnValue());
					var WorkHours = component.get("v.WorkHours");
					WorkHours.Project_name__c = currentwh.Project;
					WorkHours.Issue_Number__c = currentwh.IssueNumber;
					component.set("v.WorkHours", WorkHours);
                }else{
                      var errors = a.getError();
                      if (errors[0] && errors[0].message) 
                         console.log("Error message: " + errors[0].message); 
                }
        });
		$A.enqueueAction(ifDaysoff);   
    },
    
    saveOnchange:function (component,event,helper){
        helper.reCalc(component);
    },
    
    GetRelatedIssuesJS:function (component,event,helper){
        debugger;
        var currentwh = component.get("v.WorkHours");
   		helper.HelperGetRelatedIssues(component,currentwh.Project_name__c,currentwh.Issue_Number__c);
    },
    
    addProjectjs: function (component,event,helper){
        var ProjectToAdd = event.getParam("ProjectToAdd");
        console.log('ProjectToAdd'+ ProjectToAdd);
		var wh = component.get("v.WorkHours");
        if(ProjectToAdd != ''){
			helper.clearSelected(component);
            console.log('clear SelectedWH in addProjectjs');
            helper.addProjecthelper(component,ProjectToAdd);
        }
    },
    
    openDialog:function (component,event,helper){
    	component.set("v.PageMessage",'');
        document.getElementById("AccountPicklist").style.display = "block";
        
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById("popupProject").style.display = "block"; 
    },
	openDialogPlus:function (component,event,helper){
		var AccountId = event.getParam("AccountId");
        console.log("AccountID: "+ AccountId);
		//component.set("v.AccountId",AccountId);
        var childCmp = component.find("WorkingHoursScreenPopup")
 		childCmp.refreshProjects(AccountId);
		document.getElementById("AccountPicklist").style.display = "none";
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById("popupProject").style.display = "block"; 
	},
    addWHline:function (component,event,helper){
        var AccountId = event.getParam("AccountId");
        var PId = event.getParam("ProjectId");
        
    }
})